<!-- ==== Header === -->
<?php include('common/header.php') ?>



<!-- ==== Page_not_found_Section_start ==== -->
<section class="page_not_found_section">
<div class="container-fluid">
    <div class="row">
        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="image_area">
                <img src="images/404.png" alt="..." />
            </div>
        </div>
    </div>
</div>
</section>

<!-- ==== page_not_found_Section_End ==== -->


<!-- ==== footer === -->
<?php include('common/footer.php') ?>