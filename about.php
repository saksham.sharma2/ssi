<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame_Section_Start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area">
                    <h2>About</h2> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame_Section_End ==== -->

<!-- ==== company_profile_Section_Start ==== -->
<section class="company_profile_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_area">
                    <div class="header">
                        <h2>Company Profile</h2>
                    </div>
                    <div class="content">
                        <p>Seven Seas Petroleum LLC is a quality and service oriented company providing vital services to the Oman Oil & Gas Industry, helping national and international oil companies to develop, maintain and manage their businesses.</p>
                        <p>By applying new technology and providing a high quality service, Seven Seas Petroleum can support and partner, manufacturing and service businesses to adapt to the challenging market. Seven Seas Petroleum can provide the necessary insights and expertise which would enable client companies to maximize hydrocarbon recovery for the industry, as it focuses on Oil and Gas fields that present increasing technical challenges.</p>
                        <p>Seven Seas Petroleum has successfully diversified its business by extending its services to different sectors like Power & Energy Petrochemical, Water & Wastewater Engineering & Contacting and other related industries</p>
                        <p>The success of Seven Seas Petroleum is the result of combined efforts and skills of its people. The team's 'can-do' attitude and flexibility has been enhanced by the determination and dedication to serve the clients in the best way possible. This goes beyond the final delivery of the product or service, providing ongoing care and support, thus ensuring that the clients achieve maximum returns throughout the life of their business</p>
                        <p>As the sustained oil price drives increased exploration and development budgets, the demand for specialized services is booming. Seven Seas Petroleum has capitalized on these opportunities and made investments into the Oil & Gas Services sector providing Automation Systems, Valves, Integrated Metering Systems, Mechanical Seals and Systems, Specialty Crude Oil Pumps, People, Services and Technology to add value to the Exploration & Production (E&P) Companies.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Company_profile_Section_End ==== -->

<!-- ==== Company_value_Section_start ==== -->
<section class="company_value_section">
    <div class="container">
        <div class="row">
            <div class="col-xx-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_area">
                    <div class="header_area">
                        <h4>Company value</h4>
                        <p>At Seven Seas Petroleum LLC, we are committed to providing exceptional quality and service. We conduct our business by the following core values</p>
                    </div>
                    <div class="parent_box">
                        <div class="content_box">
                            <div class="image_1">
                                <img src="images/Vector1.png" alt="..." />
                            </div>
                            <div class="image_2">
                                <img src="images/Vector6.png" alt="..." />
                            </div>
                            <div class="number">
                                <h3>1</h3>
                            </div>
                            <div class="image_3">
                                <img src="images/Rectangle1.png" alt="...">
                            </div>
                            <div class="inner_area match_height_col">
                                <p>Professional conduct with honesty and integrity</p>
                            </div>
                            <div class="image_4">
                                <img src="images/Rectangle2.png" alt="...">
                            </div>
                        </div>
                        <div class="content_box">
                            <div class="image_1">
                                <img src="images/Vector1.png" alt="..." />
                            </div>
                            <div class="image_2">
                                <img src="images/Vector6.png" alt="..." />
                            </div>
                            <div class="number">
                                <h3>2</h3>
                            </div>
                            <div class="image_3">
                                <img src="images/Rectangle1.png" alt="...">
                            </div>
                            <div class="inner_area match_height_col">
                                <p>Professional conduct with honesty and integrity</p>
                            </div>
                            <div class="image_4">
                                <img src="images/Rectangle2.png" alt="...">
                            </div>
                        </div>
                        <div class="content_box">
                            <div class="image_1">
                                <img src="images/Vector1.png" alt="..." />
                            </div>
                            <div class="image_2">
                                <img src="images/Vector6.png" alt="..." />
                            </div>
                            <div class="number">
                                <h3>3</h3>
                            </div>
                            <div class="image_3">
                                <img src="images/Rectangle1.png" alt="...">
                            </div>
                            <div class="inner_area match_height_col">
                                <p>Professional conduct with honesty and integrity</p>
                            </div>
                            <div class="image_4">
                                <img src="images/Rectangle2.png" alt="...">
                            </div>
                        </div>
                        <div class="content_box">
                            <div class="image_1">
                                <img src="images/Vector1.png" alt="..." />
                            </div>
                            <div class="image_2">
                                <img src="images/Vector6.png" alt="..." />
                            </div>
                            <div class="number">
                                <h3>4</h3>
                            </div>
                            <div class="image_3">
                                <img src="images/Rectangle1.png" alt="...">
                            </div>
                            <div class="inner_area match_height_col">
                                <p>Professional conduct with honesty and integrity</p>
                            </div>
                            <div class="image_4">
                                <img src="images/Rectangle2.png" alt="...">
                            </div>
                        </div>
                        <div class="content_box">
                            <div class="image_1">
                                <img src="images/Vector1.png" alt="..." />
                            </div>
                            <div class="image_2">
                                <img src="images/Vector6.png" alt="..." />
                            </div>
                            <div class="number">
                                <h3>5</h3>
                            </div>
                            <div class="image_3">
                                <img src="images/Rectangle1.png" alt="...">
                            </div>
                            <div class="inner_area match_height_col">
                                <p>Professional conduct with honesty and integrity</p>
                            </div>
                            <div class="image_4">
                                <img src="images/Rectangle2.png" alt="...">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Company_value_Section_End ==== -->

<!-- ==== County_value_Section_Start ==== -->
<section class="company_profile_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_area">
                    <div class="header">
                        <h2>In County Value</h2>
                    </div>
                    <div class="content">
                        <p>Seven Seas Petroleum LLC is & has always been at the forefront in Omani Human Resource Development and maximising
                            In Country Value (ICV) for its Principals’ products and providing local services in Oman. All of the application engineering for the products 
                            sold in Oman is done locally by a team of professionals including some exceptionally talented Omani nationals. All the after-sales services delivered 
                            to our customers is via an all-Omani team of certified engineers who have been extensively trained at overseas factory locations. At Seven Seas 
                            Petroleum, we have added true value to our Principals’ products and have indigenised various offerings.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== County_value_Section_End ==== -->

<!-- ==== Parent_Section_Start ==== -->
<section class="intergrated_parent">
    <!-- ==== Seven_Seas_Petroleum_Section_Start ==== -->
    <div class="integrated_metering_section">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="inner_area">
                        <div class="row align-items-center">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="image_area">
                                    <img src="images/about1.png" alt="..." />
                                </div>
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="content_area">
                                    <div class="header_area">
                                        <h4>Seven Seas Petroleum Abu Dhabi</h4>
                                    </div>
                                    <div class="content">
                                        <p>We have established extensions to our head office to serve the GCC with our experienced resources 
                                            and services that we offer in Oman . Furthermore , we have also increased our product offering and 
                                            market size.
                                        </p>
                                    </div>
                                    <div class="view_button">
                                        <a href="javascript:;" class="btn btn-primary">View Website</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==== Seven_Seas_Petrolium_Section_End ==== -->

    <!-- ==== Intaj_LLC_Section_Start ==== -->
    <div class="integrated_metering_section msv about_top_space">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <div class="inner_area">
                        <div class="row align-items-center">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12  order-lg-0 order-1">
                                <div class="content_area align-right">
                                    <div class="header_area">
                                        <h4>INTAJ LLC</h4>
                                    </div>
                                    <div class="content">
                                        <p> INTAJ LLC is a 100 % Omani company with the primary focus on oil field development and management. 
                                            In addition, INTAJ LLC has a service arm which is involved in surface, Downhole service and 
                                            Manufacturing activities.
                                        </p>
                                    </div>
                                    <div class="view_button">
                                        <a href="javascript:;" class="btn btn-primary">View Website</a>
                                    </div>
                                </div>   
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                <div class="image_area">
                                    <img src="images/about2.png" alt="..." />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==== Intaj_LLC_Section_End ==== -->

    <!-- ==== Delta_Control_Section_Start ==== -->
    <div class="integrated_metering_section about_top_space">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="inner_area last_inner_area">
                        <div class="row align-items-center">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="image_area">
                                    <img src="images/about3.png" alt="..." />
                                </div>
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="content_area">
                                    <div class="header_area">
                                        <h4>Delta Controls FZCO</h4>
                                    </div>
                                    <div class="content">
                                        <p>Leader in the design, development, and implementation of latest technology solutions that result in reliable control automation 
                                            and information management systems.Delta Controls FZCO was established to provide turnkey solution for Controls, 
                                            Instrumentation & Automation Systems (PLC, SCADA, DCS, RTU) from Consulting, Design, Engineering to After Sales Support. 
                                            We also provide total integrated ELV solutions.
                                        </p>
                                    </div>
                                    <div class="view_button">
                                        <a href="javascript:;" class="btn btn-primary">View Website</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==== Delta_Control_Section_end ==== -->

    <!-- ==== Falcon_Technical_Services_Section_Start ==== -->
    <div class="integrated_metering_section msv about_top_space">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <div class="inner_area">
                        <div class="row align-items-center">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12  order-lg-0 order-1">
                                <div class="content_area align-right">
                                    <div class="header_area">
                                        <h4>Falcon Technical Services LLC</h4>
                                    </div>
                                    <div class="content">
                                        <p> Falcon Technical Services LLC is a Grade I company, established in 2002 as an Electro-Mechanical Engineering and Servicing 
                                            company dealing with Military, Home, Land Protection, Civil Defence and IT as its main area of operation catering Oman market. 
                                            Since its establishment FTS played a leading role in providing solution and services to many large organizations. Futhermore, 
                                            Falcon Technical Service LLC offers a broad portfolio of unique products, designed and used to protect sensitive installations 
                                            to our clients. We enhance quality, ensure compliance and reduce risk, helping you to invest time and effort where it benefits your business most
                                        </p>
                                    </div>
                                    <div class="view_button">
                                        <a href="javascript:;" class="btn btn-primary">View Website</a>
                                    </div>
                                </div>   
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                <div class="image_area">
                                    <img src="images/about2.png" alt="..." />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==== Falcon_Technical_Services_Section_End ==== -->

    <!-- ====Seven_Seas_Infotech_Section_Start ==== -->
    <div class="integrated_metering_section about_top_space">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="inner_area last_inner_area">
                        <div class="row align-items-center">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="image_area">
                                    <img src="images/about3.png" alt="..." />
                                </div>
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="content_area">
                                    <div class="header_area">
                                        <h4>Seven Seas Infotech LLC</h4>
                                    </div>
                                    <div class="content">
                                        <p>Seven Seas Infotech LLC(SSIT) is a leading System Integrator which provide complete end-to-end solution in the 
                                            field of Information Communication Technology. Our business infrastructure and human capital integrates to flower
                                            a robust performance to provide an impetus to the ICT industry on the soil of Sultanate of Oman. SSIT's activities 
                                            range from Infrastructure solutions (Servers, Storage, Disaster recovery, Structured Cabling & Networking) to 
                                            Business solutions (ERP, HRMS, CRM, Internet Websites, Intranet Portals, Document management, Business intelligence).
                                            We also provide converged ICT solutions in the field of Virtualization, UC & C, IP Telephony, IVMS, SDH/PDH, OSS/BSS, 
                                            Tower, BTS, Data Center, Video Conference, Microwave, Remote monitoring, NMS, Network forensic, Digital alarms, IP Migration etc. 
                                            We provide customized solution for various verticals like, Oil&Gas, Telecommunication, Railway, Hospitality & Ministries.
                                            With its core focus on "mission critical solutions", SSIT has number of areas of expertise which ensures business stability and enables our customers benefit from a high 
                                            level of consultancy, comprehensive technical training, after-sales support and most importantly, competitive pricing. It is not just supplying products & services;
                                            it is about developing an understanding of every single customer we work for.
                                        </p>
                                    </div>
                                    <div class="view_button">
                                        <a href="javascript:;" class="btn btn-primary">View Website</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==== Delta_Control_Section_end ==== -->
</section>
<!-- ==== Parent_Section_End ==== -->




<!-- ==== footer === -->
<?php include('common/footer.php') ?>