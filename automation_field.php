<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame_section_start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area">
                    <h2>AUTOMATION FIELD SERVICES</h2> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame_section_end ==== -->

<section class="intergrated_parent">
    <!-- ==== control_system_services_section_Start ==== -->
    <div class="integrated_metering_section">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="inner_area mechanical_seal_inner_area">
                        <div class="row align-items-center">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="image_area">
                                    <img src="images/integ1.png" alt="..." />
                                </div>
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="content_area">
                                    <div class="header_area">
                                        <h4>Control system services</h4>
                                    </div>
                                    <div class="content">
                                        <p>Installation, Commisioning and maintenance for the various instruments 
                                            such as temperature transmitters, pressure transmitters, level 
                                            transmitters(Guided wave Radar and pressure type level Transmitters). 
                                            Flow Transmitters (Coriolis Flow Meter, Magnetic Flow Meter), Gas Chromotograph, 
                                            Flow Computers,(S600,S600+, Floboss 107 and ROC 800), Wireless(Gateway, Wireless 
                                            Transmitters and wired Transmitters used with THUMB) and skids used for batch 
                                            processing as well as for Fiscal metering.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==== Control_system_services_section_end ==== -->

    <!-- ==== Instrumentation_Service_section_Start ==== -->
    <div class="crane_service_section">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="content_area">
                        <div class="header">
                            <h3>Instrumentation Services</h3>
                            <p>The SSP System services division includes experienced engineering resources to undertake 
                                the full spectrum of automation project execution services. Local support is the key factor for 
                                selecting a supplier,vendor, for which sets us apart from the competition. We are closely associated 
                                with the customer long after the process has started up. Seven Seas Petroleum supports all cusomers 
                                in Oman for Emerson's Deltav Process Control System & DeltaV SIS for the following.</p>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12">
                                <div class="content_box">
                                    <div class="inner_area match_height_col">
                                        <p>Control System Design & Engineering</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12">
                                <div class="content_box">
                                    <div class="inner_area match_height_col">
                                        <p>Control System Installation & Commisioning</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12">
                                <div class="content_box">
                                    <div class="inner_area match_height_col">
                                        <p>Modification & Expansion Of Control Systems</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12">
                                <div class="content_box">
                                    <div class="inner_area match_height_col">
                                        <p>Preventive Maintenance</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12">
                                <div class="content_box">
                                    <div class="inner_area match_height_col">
                                        <p>Corrective Maintenance</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12">
                                <div class="content_box">
                                    <div class="inner_area match_height_col">
                                        <p>Emergency Call Site Support</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-4 col-sm-4 col-12">
                                <div class="content_box">
                                    <div class="inner_area match_height_col">
                                        <p>Emergency HelpDesk Support</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- ==== Instrumentation_Service_section_end ==== -->
</section>
<!-- ==== footer === -->
<?php include('common/footer.php') ?>