<!-- ==== Header === -->
<?php include('common/header.php') ?>


<!-- ==== Breadcame_Section_Start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area">
                    <h2>Career</h2> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame_Section_End ==== -->

<!-- ==== Banner_Section_Start ==== -->
<section class="team_banner_section" style="background-image:url('./images/career.png')">
</section>
<!-- ==== Banner_Section_End ==== -->

<!-- ==== Join_Team_Section_Start ==== -->
<section class="jointeam_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_area">
                    <h2>Join our team</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem 
                        ipsum dolor sit amet, consectetur adipiscing elit. ipsum dolor sit amet, 
                        consectetur adipiscing elit.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Join_Team_Section_End ==== -->

<!-- ==== Management_Section_Start ==== -->
<section class="management_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_box_area">
                    <div class="row">
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="content_box">
                                <div class="header_area">
                                    <div class="left">
                                        <p>Management</p>
                                    </div>
                                    <div class="right">
                                        <p>3 days ago</p>
                                    </div>
                                </div>
                                <div class="content">
                                    <p>Lorem ipsum dolor sit amet</p>
                                    <ul>
                                        <li><i class="far fa-map-marker-alt pe-2"></i>Oman</li>
                                        <li><i class="fal fa-clock pe-2"></i>Full time</li>
                                    </ul>
                                    <div class="inner_area">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, 
                                            consectetur adipiscing elit. ipsum dolor sit amet, consectetur adipiscing elit.
                                            <a href="javascript:;"  data-bs-toggle="modal" data-bs-target="#join_team">Read more</a>
                                        </p>
                                    </div>
                                    <div class="apply_button">
                                        <a href="javascript:;" class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#apply">Apply now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="content_box">
                                <div class="header_area">
                                    <div class="left">
                                        <p>Management</p>
                                    </div>
                                    <div class="right">
                                        <p>3 days ago</p>
                                    </div>
                                </div>
                                <div class="content">
                                    <p>Lorem ipsum dolor sit amet</p>
                                    <ul>
                                        <li><i class="far fa-map-marker-alt pe-2"></i>Oman</li>
                                        <li><i class="fal fa-clock pe-2"></i>Full time</li>
                                    </ul>
                                    <div class="inner_area">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, 
                                            consectetur adipiscing elit. ipsum dolor sit amet, consectetur adipiscing elit.
                                            <a href="javascript:;"  data-bs-toggle="modal" data-bs-target="#join_team">Read more</a>
                                        </p>
                                    </div>
                                    <div class="apply_button">
                                        <a href="javascript" class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#apply">Apply now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="content_box">
                                <div class="header_area">
                                    <div class="left">
                                        <p>Human resource</p>
                                    </div>
                                    <div class="right">
                                        <p>6 days ago</p>
                                    </div>
                                </div>
                                <div class="content">
                                    <p>Lorem ipsum dolor sit amet</p>
                                    <ul>
                                        <li><i class="far fa-map-marker-alt pe-2"></i>Oman</li>
                                        <li><i class="fal fa-clock pe-2"></i>Part time</li>
                                    </ul>
                                    <div class="inner_area">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, 
                                            consectetur adipiscing elit. ipsum dolor sit amet, consectetur adipiscing elit.
                                            <a href="javascript:;"  data-bs-toggle="modal" data-bs-target="#join_team">Read more</a>
                                        </p>
                                    </div>
                                    <div class="apply_button">
                                        <a href="javascript" class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#apply">Apply now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="content_box">
                                <div class="header_area">
                                    <div class="left">
                                        <p>Mechanical</p>
                                    </div>
                                    <div class="right">
                                        <p>1 month ago</p>
                                    </div>
                                </div>
                                <div class="content">
                                    <p>Lorem ipsum dolor sit amet</p>
                                    <ul>
                                        <li><i class="far fa-map-marker-alt pe-2"></i>Oman</li>
                                        <li><i class="fal fa-clock pe-2"></i>Full time</li>
                                    </ul>
                                    <div class="inner_area">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, 
                                            consectetur adipiscing elit. ipsum dolor sit amet, consectetur adipiscing elit.
                                            <a href="javascript:;"  data-bs-toggle="modal" data-bs-target="#join_team">Read more</a>
                                        </p>
                                    </div>
                                    <div class="apply_button">
                                        <a href="javascript" class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#apply">Apply now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="content_box">
                                <div class="header_area">
                                    <div class="left">
                                        <p>Management</p>
                                    </div>
                                    <div class="right">
                                        <p>3 months ago</p>
                                    </div>
                                </div>
                                <div class="content">
                                    <p>Lorem ipsum dolor sit amet</p>
                                    <ul>
                                        <li><i class="far fa-map-marker-alt pe-2"></i>Oman</li>
                                        <li><i class="fal fa-clock pe-2"></i>Full time</li>
                                    </ul>
                                    <div class="inner_area">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, 
                                            consectetur adipiscing elit. ipsum dolor sit amet, consectetur adipiscing elit.
                                            <a href="javascript:;"  data-bs-toggle="modal" data-bs-target="#join_team">Read more</a>
                                        </p>
                                    </div>
                                    <div class="apply_button">
                                        <a href="javascript" class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#apply">Apply now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="content_box">
                                <div class="header_area">
                                    <div class="left">
                                        <p>Management</p>
                                    </div>
                                    <div class="right">
                                        <p>3 months ago</p>
                                    </div>
                                </div>
                                <div class="content">
                                    <p>Lorem ipsum dolor sit amet</p>
                                    <ul>
                                        <li><i class="far fa-map-marker-alt pe-2"></i>Oman</li>
                                        <li><i class="fal fa-clock pe-2"></i>Full time</li>
                                    </ul>
                                    <div class="inner_area">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, 
                                            consectetur adipiscing elit. ipsum dolor sit amet, consectetur adipiscing elit.
                                            <a href="javascript:;"  data-bs-toggle="modal" data-bs-target="#join_team">Read more</a>
                                        </p>
                                    </div>
                                    <div class="apply_button">
                                        <a href="javascript" class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#apply">Apply now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Management_Section_End ==== -->







<!-- ==== footer === -->
<?php include('common/footer.php') ?>