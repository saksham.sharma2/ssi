<section class="footer_logo_section">
	<div class="container">
		<div class="row">
			<div class="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 mx-auto">
				<div class="logo_area">
					<div class="logo">
						<a href="javascript:;"><img src="images/footerlogo.png" alt="..." /></a>
					</div>
					<div class="content">
						<p>Seven Seas Petroleum LLC is a Quality & Superior Service oriented company
							established in 2002. The company was set up to provide Oman's Oil Producers 
							and Ministries with Oil field instrumentation and Mechanical equipment from 
							some of the world's top manufacturing companies.
						</p>
					</div>
					<div class="social_icon">
						<ul>
							<li>
								<a href="javascript:;"><i class="fab fa-facebook"></i></a>
							</li>
							<li>
								<a href="javascript:;"><i class="fab fa-linkedin"></i></a>
							</li>
							<li>
								<a href="javascript:;"><i class="fab fa-youtube"></i></a>
							</li>
						</ul>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>

<section class="footer_section">
	<div class="container">
		<div class="main_footer">
			<div class="row">
				<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="row">
						<div class="col-xxl-8 col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
							<div class="row">
								<div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
									<div class="footer_area">
										<div class="branches_area">
											<h6>Headquarters</h6>
											<p>P.O. Box 2648, Postal Code 112, Ruwi,</p>
											<p>Sultanate of Oman</p>
											
											<a href="tel:96824212222">T. +968 24212222</a>
											<a href="tel:96824629903">F. +968 24629903</a>
										</div>
										<div class="branches_area bottom_padding">
											<h6>Automation Centre Ghala</h6>
											<p>Building No. 86, Way No. 54, Block No. 264,</p>
											<p>	Ghala Industrial Area, Muscat, Sultanate of</p>
											<p>Oman</p>
										</div>
										<div class="branches_area bottom_padding">
											<h6>Seven Seas Petroleum Logistics</h6><h6> Center</h6>
											<p>Ghala Industrial Area, Bldg 999/22 ST No.</p>
											<p>9999,Block No. 264</p>
											</p>
											<a href="tel:96824591520">T. +96824591520</a>
											<a href="tel:96893993768">M. +96893993768</a>
										</div>
									</div>
								</div>
								<div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
									<div class="footer_area">
										<div class="listing_area">
											<div class="header">
												<h3>Company</h3>
											</div>
											<div class="inner_area">
												<ul>
													<li>
														<a href="javascript:;">Services</a>
													</li>
													<li>
														<a href="javascript:;">Projects</a>
													</li>
													<li>
														<a href="our_team.php">Team behind</a>
													</li>
													<li>
														<a href="career.php">Job careers</a>
													</li>
													<li>
														<a href="news.php">News</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<div class="col-xxl-3 col-xl-3 col-lg-3 col-md-3 col-sm-3 col-6">
									<div class="footer_area">
										<div class="listing_area">
											<div class="header">
												<h3>Support</h3>
											</div>
											<div class="inner_area">
												<ul>
													<li>
														<a href="about.php">About</a>
													</li>
													<li>
														<a href="testimony.php">Testimony</a>
													</li>
													<li>
														<a href="contact_us.php">Contact us</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
							<div class="footer_area">
								<div class="form_area">
									<div class="header">
										<h5>Send us a message</h5>
										<h2>Get In Touch</h2>
									</div>
									<div class="inner_area">
										<div class="row">
											<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
												<form>
													<div class="row">
														<div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
															<div class="form-group">
																<input type="text" class="form-control" placeholder="Name" autocomplete="off" />
															</div>
														</div>
														<div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
															<div class="form-group">
																<input type="number" class="form-control" placeholder="Phone number" autocomplete="off" />
															</div>
														</div>
														<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
															<div class="form-group">
																<input type="email" class="form-control" placeholder="Email address" autocomplete="off" />
															</div>
														</div>
														<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
															<div class="form-group">
																<textarea class="form-control" placeholder="Tell us your needs..." autocomplete="off"></textarea>
															</div>
														</div>
														<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
															<div class="submit_btn">
																<a href="javascript:;" class="btn btn-primary-1">Submit</a>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<?php include('common/modal.php') ?>
<!-- ====jQuery suffle_plugins === -->
<script src="https://unpkg.com/shufflejs@5"></script>
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
<!-- ==== jQuery JS ==== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<!-- ==== Bootstrap JS ==== -->
<script src="plugins/owl2/js/owl.carousel.js"></script>
<script src="plugins/owl2/js/owl-custom.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- ==== Owl Carausel Js ==== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<!-- ==== jQuery validation JS ==== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
<!-- ==== jQuery Additional method validation JS ==== -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/additional-methods.min.js"></script>

<!-- ====JS custome === -->
<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>