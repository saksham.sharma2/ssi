<section class="footer_form">
    <div class="maps">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.714259819528!2d58.3743145154296!3d23.57870380122945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e91ffd75ae2f525%3A0xba7e54290d8f6b76!2sGhala%20Industrial%20Area!5e0!3m2!1sen!2sin!4v1650874645334!5m2!1sen!2sin" width="100%" height="500" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        <div class="get_it">
            <div class="container">
                <div class="row">
                    <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-8 col-sm-8 col-12">
                        <div class="form_box">
                            <div class="header_area">
                                <h4>Get In Touch</h4>
                                <h3>SEND US A MESSAGE</h3>
                            </div>
                            <form>
                                <div class="row">
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" autocomplete="off" placeholder="Your Name" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" autocomplete="off" placeholder="Email Address" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-6 col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" autocomplete="off" placeholder="Phone Number" />
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="form-group">
                                            <textarea type="text" class="form-control" autocomplete="off" placeholder="Tell us your needs.."></textarea>
                                        </div>
                                    </div>
                                    <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="button">
                                            <a href="javascript:;">SUBMIT NOW</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12  d-lg-block  d-none">
                        <!-- <div class="img_area">
                            <img src="images/service1.jpg" alt=".." />
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>