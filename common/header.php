<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>SSP</title>

	<link rel="icon" href="images/logo.png" type="image/x-icon" />
	<!-- ==== Bootstrap CSS ==== -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.1.3/css/bootstrap.min.css" integrity="sha512-GQGU0fMMi238uA+a/bdWJfpUGKUkBdgfFdgBm72SUQ6BeyWjoY/ton0tEjH+OSH9iP4Dfh+7HM0I9f5eR0L/4w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<!-- ==== poppins	 Fonts ==== -->
	<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<!-- ==== Font Awesome CSS ==== -->
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.15.4/css/all.css" />
	<!-- ==== Owl Carausel CSS ==== -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" integrity="sha512-UTNP5BXLIptsaj5WdKFrkFov94lDx+eBvbKyoe1YAfjeRPC+gT5kyZ10kOHCfNZqEui1sxmqvodNUx3KbuYI/A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<!-- ==== Custom CSS ==== -->
	<link rel="stylesheet" href="css/custom.css" />
	<!-- ==== owl carousel 2==== -->
    <link rel="stylesheet" href="plugins/owl2/css/owl.carousel.css">
	<!-- ==== Fancy box ==== -->
	<link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css"
    />
</head>
<body id="main">
<section class="header_top_section">
	<div class="header_wraper">
		<div class="container">
			<div class="row">
				<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="contact">
						<ul>
							<li><a href="tel:96824212222"><i class="fas fa-phone-alt"></i>+968 24212222</a></li>
							
							<li>			
								<a href="mailto:info@sevenseaspetroleum.com"><i class="fal fa-envelope"></i>info@sevenseaspetroleum.com</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
	
<section class="header-main-section ">
	<div class="header_menu_wraper">
		<div class="container">
			<div class="row">
				<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
					<div class="head_navigation  d-xl-block d-none">
						<div class="header_area">
							<div class="left_area ">
								<div class="logo">
									<a href="index.php">
										<img src="images/logo.png" alt="..." />
									</a>
								</div>
							</div>
							<div class="right_area">
								<div class="menu_box">
									<div class="inner_area">	
										<div class=" navbar-collapse" id="main_nav">
											<ul class="">
												<li class="nav-item active"> <a class="nav-link" href="about.php">About</a> </li>
												<li class="nav-item dropdown" id="myDropdown1">
													<a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">What we do</a>
													<ul class="dropdown-menu show">
														<li> <a class="dropdown-item" href="javascript:;">Our Business<i class="fal fa-angle-right"></i></a>
															<ul class="submenu dropdown-menu">
																<li><a class="dropdown-item" href="our_business.php">Instrumentation and automation</a></li>
																<li><a class="dropdown-item" href="engineered_solution.php">Engineered solutions</a></li>
															</ul>
														</li>
														<li> <a class="dropdown-item" href="javascript:;">Our Services <i class="fal fa-angle-right"></i></a>
															<ul class="submenu dropdown-menu">
																<li><a class="dropdown-item" href="mechanical_seal.php">Mechanical Seal Refurbishment center</a></li>
																<li><a class="dropdown-item" href="integration_services.php">Integration services</a></li>
																<li><a class="dropdown-item" href="automation_field.php">Automation field services</a></li>
															</ul>
														</li>
														<li> <a class="dropdown-item" href="javascript:;">Our Partners</a></li>
													</ul>
												</li>
												<li class="nav-item dropdown" id="myDropdown2">
													<a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Projects</a>
													<ul class="dropdown-menu show">
														<li> <a class="dropdown-item" href="complete_project.php">Completed Projects</a></li>
														<li> <a class="dropdown-item" href="under_execution.php">Under Execution</a></li>
													</ul>
												</li>
												<li class="nav-item active"> <a class="nav-link" href="testimony.php">Testimony</a> </li>
												<li class="for_secrh">
													<div class="open">
														<a href="javascript:;" class="serch_open"><i class="fal fa-search"></i> <i class="fal fa-times"></i></a>
													</div>
												</li>
												<li class="nav-item active"> 
													<div class="search_box">
														<form>
															<div class="form-group">
																<input type="text" class="form-control" placeholder="Search" />
															</div>
															<div class="icon">
																<a href="javascript:;"  class="opened"><i class="far fa-search"></i></a>
															</div>
														</form>
													</div>
												</li>
												<li class="button"> <a href="javascript:;" class="btn btn-primary">Contact</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
					<div class="responsive_menu  d-xl-none d-block">
						<div class="menu_boxes">
							<div class="logo">
								<div class="img_logo">
									<a href="index.php">
										<img src="images/logo.png" alt="..." />
									</a>
								</div>
							</div>
							<div class="right">
								<div class="search_box">
									<form>
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Search" />
										</div>
										<div class="icon">
											<a href="javascript:;"  class="opened"><i class="far fa-search"></i></a>
										</div>
									</form>
								</div>
								<div class="open_close">
									<a href="javascript:;" class="res_menubar">
										<i class="fas fa-bars"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="open_menu">
							<div class="top_area">
								<div class="logo_box">
									<a href="index.php">
										<img src="images/logo.png" alt="..." />
									</a>
								</div>
								<div class="cross_menu">
									<a href="javascript:;">
										<i class="far fa-times"></i>
									</a>
								</div>
							</div>
							<div class="inner_res_menu inner_sidebar">
							<div class=" navbar-collapse" id="main_nav">
									<ul class="">
										<li class="nav-item active"> <a class="nav-link" href="about.php">About</a> </li>
										<li class="nav-item dropdown" id="myDropdown1">
											<a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">What we do</a>
											<ul class="dropdown-menu show">
												<li> <a class="dropdown-item dropdown_icon" href="javascript:;">Our Business</a>
													<ul class="submenu dropdown-menu">
														<li><a class="dropdown-item" href="our_business.php">Instrumentation and automation</a></li>
														<li><a class="dropdown-item" href="engineered_solution.php">Engineered solutions</a></li>
													</ul>
												</li>
												<li> <a class="dropdown-item dropdown_icon" href="javascript:;">Our Services </a>
													<ul class="submenu dropdown-menu">
														<li><a class="dropdown-item" href="mechanical_seal.php">Mechanical Seal Refurbishment center</a></li>
														<li><a class="dropdown-item" href="integration_services.php">Integration services</a></li>
														<li><a class="dropdown-item" href="automation_field.php">Automation field services</a></li>
													</ul>
												</li>
												<li> <a class="dropdown-item" href="javascript:;">Our Partners</a></li>
											</ul>
										</li>
										<li class="nav-item dropdown" id="myDropdown2">
											<a class="nav-link dropdown-toggle" href="#" data-bs-toggle="dropdown">Projects</a>
											<ul class="dropdown-menu">
												<li> <a class="dropdown-item" href="complete_project.php">Completed Projects</a></li>
												<li> <a class="dropdown-item" href="under_execution.php">Under Execution</a></li>
											</ul>
										</li>
										<li class="nav-item active"> <a class="nav-link" href="testimony.php">Testimony</a> </li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>