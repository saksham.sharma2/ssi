

<!-- ==== Apply for team modal  ==== -->

<div class="modal el_modal apply_for fade" id="apply" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
                <h5 class="modal-title">Apply for this position</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="apply_for">
                    <form>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <input type="text" class="form-control" name="" placeholder="Name">
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <input type="email" class="form-control" name="" placeholder="Email">
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <textarea type="text" class="form-control" name="" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <div class="icon_add">
                                    <div class="upload-image-section">
                                        <div class="upload-section">
                                            <div class="button-ref">
                                                <button class="btn btn-icon btn-primary btn-lg" type="button">
                                                    <span class="btn-inner--icon"><i class="fas fa-upload"></i></span>
                                                    <span class="btn-inner--text">Upload Image</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="submit_button">
                                <a href="javascript:;" class="btn btn-primary w-100">Submit</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ==== Join_team modal  ==== -->
<div class="modal el_modal join_team fade" id="join_team" data-bs-backdrop="static" data-bs-keyboard="false">
    <div class="modal-dialog  modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="join_team">
                    <div class="content_box">
                        <div class="header_area">
                            <div class="left">
                                <p>Management</p>
                            </div>
                            <div class="right">
                                <p>3 days ago</p>
                            </div>
                        </div>
                        <div class="content">
                            <p>Lorem ipsum dolor sit amet</p>
                            <div class="icon_area">
                                <ul>
                                    <li><i class="far fa-map-marker-alt pe-2"></i>Oman</li>
                                    <li><i class="fal fa-clock pe-2"></i>Full time</li>
                                </ul>
                            </div>
                            <div class="inner_area">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, 
                                    consectetur adipiscing elit. ipsum dolor sit amet, consectetur adipiscing elit.
                                </p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, 
                                    consectetur adipiscing elit. ipsum dolor sit amet, consectetur adipiscing elit.
                                </p>
                                <ul>
                                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </li>
                                    <li>Lorem ipsum dolor sit amet, consectetur Lorem ipsum</li>
                                    <li>Consectetur adipiscing elit lorem ipsum dolor sit amet.</li>
                                    <li>Consectetur adipiscing elit. ipsum dolor sit amet,</li>
                                </ul>
                            </div>
                            <div class="apply_button">
                                <a href="javascript:;" class="btn btn-primary w-100" data-bs-toggle="modal" data-bs-target="#apply">Apply now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>