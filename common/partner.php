<section class="footer_image">
	<div class="container">
		<div class="row">
			<div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
				<div class="images_area">
					<ul>
						<li>
							<div class="img_area">
								<img src="images/logo/logo1.png" alt="..." />
							</div>
						</li>
						<li>
							<div class="img_area">
								<img src="images/logo/logo2.png" alt="..." />
							</div>
						</li>
						<li>
							<div class="img_area">
								<img src="images/logo/logo3.png" alt="..." />
							</div>
						</li>
						<li>
							<div class="img_area">
								<img src="images/logo/logo4.png" alt="..." />
							</div>
						</li>
						<li>
							<div class="img_area">
								<img src="images/logo/logo5.png" alt="..." />
							</div>
						</li>
						<li>
							<div class="img_area">
								<img src="images/logo/logo6.png" alt="..." />
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
