<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame_section_start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area">
                    <h2>COMPLETED PROJECTS</h2> 
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== Breadcame_section_end ==== -->

<!-- ==== Compelete_project_Section_Start ==== -->
<section class="complete_project_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_box_area">
                    <div class="row my-shuffle-container">
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 picture-item">
                            <div class="content_box">
                                <div class="image">
                                    <img src="images/project1.png" alt="..." />
                                </div>
                                <div class="content">
                                    <div class="inner_area">
                                        <h4> Project 1</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                            purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                        </p>
                                        <a href="complete_project_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 picture-item">
                            <div class="content_box">
                                <div class="image">
                                    <img src="images/project2.png" alt="..." />
                                </div>
                                <div class="content">
                                    <div class="inner_area">
                                        <h4> Project 2</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                            purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                        </p>
                                        <a href="complete_project_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 picture-item">
                            <div class="content_box">
                                <div class="image">
                                    <img src="images/project3.png" alt="..." />
                                </div>
                                <div class="content">
                                    <div class="inner_area">
                                        <h4> Project 3</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                            purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                        </p>
                                        <a href="complete_project_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 picture-item">
                            <div class="content_box">
                                <div class="image">
                                    <img src="images/project4.png" alt="..." />
                                </div>
                                <div class="content">
                                    <div class="inner_area">
                                        <h4> Project 4</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                            purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                        </p>
                                        <a href="complete_project_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 picture-item">
                            <div class="content_box">
                                <div class="image">
                                    <img src="images/project5.png" alt="..." />
                                </div>
                                <div class="content">
                                    <div class="inner_area">
                                        <h4> Project 5</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                            purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                        </p>
                                        <a href="complete_project_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 picture-item">
                            <div class="content_box">
                                <div class="image">
                                    <img src="images/project1.png" alt="..." />
                                </div>
                                <div class="content">
                                    <div class="inner_area">
                                        <h4> Project 6</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                            purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                        </p>
                                        <a href="complete_project_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12 my-sizer-element"></div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Compelete_projects_Section_End ==== -->



<!-- ==== footer === -->
<?php include('common/footer.php') ?>