<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame_section_start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area">
                    <h2>CONTACT US</h2> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame_section_end ==== -->
<!-- ==== Map_section_Start ==== -->
<section class="map_section">
    <div class="maps">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.714259819528!2d58.3743145154296!3d23.57870380122945!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e91ffd75ae2f525%3A0xba7e54290d8f6b76!2sGhala%20Industrial%20Area!5e0!3m2!1sen!2sin!4v1650874645334!5m2!1sen!2sin"  allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
</section>
<!-- ==== Map_section_end ==== -->

<!-- ==== stay_in_touch_section_start ==== -->
<section class="stay_in_touch_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-10 col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12 mx-auto">
                <div class="content_box">
                    <div class="inner_area">
                        <div class="row">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="left_area">
                                    <div class="content">
                                        <h4>Call or visit us at one of our different location </h4>
                                    </div>
                                    <div class="map_img d-md-block d-none">
                                        <img src="images/map_img.png" alt="..." />
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <div class="right_area">
                                    <div class="content">
                                        <div class="left">
                                            <div class="flag">
                                                <img src="images/flag1.png" alt="..." />
                                            </div>
                                        </div>
                                        <div class="right">
                                            <h4>Headquarters</h4>
                                            <p>P.O. Box 2648, Ruwi Postal Code 112 Sultanate of Oman</p>
                                            <ul>
                                            <li><a href="tel:96824212222">Tel: +968 24212222</a></li>
                                            <li><a href="tel:96824629903">Fax: +968 24629903</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="left">
                                            <div class="flag">
                                                <img src="images/flag1.png" alt="..." />
                                            </div>
                                        </div>
                                        <div class="right">
                                            <h4>Seven Seas Petroleum Store</h4>
                                            <p>Ghala Industrial Area Bldg 999/22 ST No: 9999
                                                Block No: 264
                                            </p>
                                            <ul>
                                            <li><a href="tel:96824591520">Tel: +968 24 591520</a></li>
                                            <li><a href="tel:96893993768">Mobile: +968 93993768</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="content">
                                        <div class="left">
                                            <div class="flag">
                                                <img src="images/flag2.png" alt="..." />
                                            </div>
                                        </div>
                                        <div class="right remove_border">
                                            <h4>Seven Seas Petroleum Abu Dhabi office</h4>
                                            <p>Seven Seas Petroleum LLC | P.O. Box 25237, Abu Dhabi, UAE</p>
                                            <p>Seven Seas Petroleum LLC, 803, 8th Floor,
                                                Al Sorough Building P.O. Box 25237, Abu Dhabi, UAE</p>
                                            <ul>
                                            <li><a href="tel:97124456636">Tel: +971 2 4456636</a></li>
                                            <li><a href="tel:97124456637">Fax: +971 24456637</a></li>
                                            <li><a href="tel:971556865002">Mobile: +971 556865002</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="in_touch">
                        <p>Stay in touch</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== stay_in_touch_section_end ==== -->





<!-- ==== footer === -->
<?php include('common/footer.php') ?>