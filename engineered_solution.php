<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame_Section_Start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area">
                    <h2>OUR BUSINESS</h2> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame_Section_End ==== -->

<!-- ==== Engineered_solutions_Section_Start ==== -->
<section class="engineered_solution_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_area">
                    <div class="header">
                        <h3>Engineered solutions</h3>
                        <p>Engineered Solutions is one of the three verticals of Seven Seas Petroleum that 
                            serves the local industry with custom built capital equipment. It leverages the  existing 
                            strong relationships that  Seven  Seas  Petroleum has  with  many world  renowned  
                            manufacturers &  solution providers  over several years and also develop new relationships 
                            in order to cater to the ever growing Oman industrial base.
                        </p>
                        <p>There are five broad subdivisions in the Engineered Solutions Vertical:</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Engineered_solutions_Section_End ==== -->

<!-- ==== Circle_solutions_Section_Start ==== -->
<section class="circle_solution_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_area">
                    <div class="row">
                        <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="content_box">
                                <div class="inner_area match_height_col">
                                    <p>Mechanical rotating equipment covering gas compressors, Pumps, 
                                        Mechanical seals, couplings, flare gas recovery packages.</p>
                                </div>
                            </div>
                            <div class="content_box">
                                <div class="inner_area match_height_txt">
                                    <p>Mechanical static equipment covering seamless CRA pipes and tubes. 
                                        Cladded pipes. Ejector solutions for flare gas recovery and production boosting.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 d-lg-block d-none">
                            <div class="circle_area">
                                <div class="inner_area">
                                    <div class="image">
                                        <img src="images/chairman.png" alt="..." />
                                        <div class=" circle circle_1"><p>1</p></div>
                                        <div class="circle circle_2"><p>2</p></div>
                                        <div class="circle circle_3"><p>3</p></div>
                                        <div class="circle circle_4"><p>4</p></div>
                                        <div class="circle circle_5"><p>5</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="content_box">
                                <div class="inner_area match_height_col">
                                    <p>Automation and electrical covering PLC based automation systems. Electrical automation systems. 
                                        Production technologies. And electrical equipment.</p>
                                </div>
                            </div>
                            <div class="content_box">
                                <div class="inner_area match_height_txt">
                                    <p>Process equipment such as gas conditioning/Filtration/Sweetening/dehydration units. Produced water treatment packages. 
                                        Oil  and gas train equipment and early production facilities.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 mx-auto">
                            <div class="content_box">
                                <div class="inner_area match_height_col">
                                    <p>Automation and electrical covering PLC based automation systems. Electrical automation systems. 
                                        Production technologies. And electrical equipment.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Cirle_solutions_Section_End ==== -->

<!-- ==== Accordian_Section_Start ==== -->
<section class="accordian_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="solutions_detail_area">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Mechanical – Rotating
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse show"
                                aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex
                                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse
                                        augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                                        Nam quis nulla sollicitudin enim scelerisque molestie. Vitae malesuada felis
                                        ex tempus diam. Etiam tristique euismod Suspendisse potenti.
                                        Lorem ipsum dolor sit amet. Suspendisse augue purus, hendrerit vel lacus at,
                                        aliquam eleifend lacus. Nam quis nulla sollicitudin enim scelerisque
                                        molestie.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex tempus diam.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex
                                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse
                                        augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                                        Nam quis nulla sollicitudin enim scelerisque molestie. Vitae malesuada felis
                                        ex tempus diam. Etiam tristique euismod Suspendisse potenti.
                                        Lorem ipsum dolor sit amet. Suspendisse augue purus, hendrerit vel lacus at,
                                        aliquam eleifend lacus. Nam quis nulla sollicitudin enim scelerisque
                                        molestie.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex tempus diam.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Mechanical – Static
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex
                                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse
                                        augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                                        Nam quis nulla sollicitudin enim scelerisque molestie. Vitae malesuada felis
                                        ex tempus diam. Etiam tristique euismod Suspendisse potenti.
                                        Lorem ipsum dolor sit amet. Suspendisse augue purus, hendrerit vel lacus at,
                                        aliquam eleifend lacus. Nam quis nulla sollicitudin enim scelerisque
                                        molestie.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex tempus diam.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapseThree" aria-expanded="false"
                                    aria-controls="collapseThree">
                                    Integrated Mechanical Skids 
                                </button>
                            </h2>
                            <div id="collapseThree" class="accordion-collapse collapse"
                                aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p>1. WHCP Well Head Hydraulic & Control  Panels.</p>
                                    <p>2. Chemical injection Skids and sampling  bomb assemblies.</p>
                                    <p>3. High Pressure Sample  Cylinder.</p>  
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading4">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                    Process Equipment
                                </button>
                            </h2>
                            <div id="collapse4" class="accordion-collapse collapse" aria-labelledby="heading4"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex
                                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse
                                        augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                                        Nam quis nulla sollicitudin enim scelerisque molestie. Vitae malesuada felis
                                        ex tempus diam. Etiam tristique euismod Suspendisse potenti.
                                        Lorem ipsum dolor sit amet. Suspendisse augue purus, hendrerit vel lacus at,
                                        aliquam eleifend lacus. Nam quis nulla sollicitudin enim scelerisque
                                        molestie.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex tempus diam.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="heading5">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                    Automation and electrical soultions 
                                </button>
                            </h2>
                            <div id="collapse5" class="accordion-collapse collapse" aria-labelledby="heading5"
                                data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex
                                        tempus diam. Etiam tristique euismod ante, vel ornare urna. Suspendisse
                                        augue purus, hendrerit vel lacus at, aliquam eleifend lacus.
                                        Nam quis nulla sollicitudin enim scelerisque molestie. Vitae malesuada felis
                                        ex tempus diam. Etiam tristique euismod Suspendisse potenti.
                                        Lorem ipsum dolor sit amet. Suspendisse augue purus, hendrerit vel lacus at,
                                        aliquam eleifend lacus. Nam quis nulla sollicitudin enim scelerisque
                                        molestie.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam lobortis
                                        dolor mauris. Nullam interdum, ectus, vitae malesuada felis ex tempus diam.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Accordian_Section_End ==== -->

<!-- ==== Sustainable_Energy_Section_Start ==== -->
<section class="company_value_section hide_bgcolor">
    <div class="container">
        <div class="row">
            <div class="col-xx-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_area">
                    <div class="header_area">
                        <h4>Sustainable Energy</h4>
                        <p>As part  of the  diversification strategy  and  catering to various  market  segments, Seven Seas Petroleum has established  a separate team specifically engaged in promoting various sustainable  energy  solutions and  services.
                            Seven   Seas Petroleum is   actively participating in project coordination and execution of utility scale renewable energy   projects. In addition, we have   the capability of operating and maintaining (O&M) Solar P V plants with renowned partners.
                            Oman has set targets   to increase renewable energy penetration to essentially reduce emissions. Seven Seas Petroleum has invested and partnered with cutting edge technology providers to serve this specific segment.
                        </p>
                    </div>
                    <div class="parent_box">
                        <div class="content_box color_change">
                            <div class="image_1">
                                <img src="images/vector14.png" alt="..." />
                            </div>
                            <div class="image_2">
                                <img src="images/vector13.png" alt="..." />
                            </div>
                            <div class="number">
                                <h3>1</h3>
                            </div>
                            <div class="image_3">
                                <img src="images/vector11.png" alt="...">
                            </div>
                            <div class="inner_area match_height_col">
                                <p>Professional conduct with honesty and integrity</p>
                            </div>
                            <div class="image_4">
                                <img src="images/vector12.png" alt="...">
                            </div>
                        </div>
                        <div class="content_box color_change">
                            <div class="image_1">
                                <img src="images/vector14.png" alt="..." />
                            </div>
                            <div class="image_2">
                                <img src="images/vector13.png" alt="..." />
                            </div>
                            <div class="number">
                                <h3>2</h3>
                            </div>
                            <div class="image_3">
                                <img src="images/vector11.png" alt="...">
                            </div>
                            <div class="inner_area match_height_col">
                                <p>Professional conduct with honesty and integrity</p>
                            </div>
                            <div class="image_4">
                                <img src="images/vector12.png" alt="...">
                            </div>
                        </div>
                        <div class="content_box color_change">
                            <div class="image_1">
                                <img src="images/vector14.png" alt="..." />
                            </div>
                            <div class="image_2">
                                <img src="images/vector13.png" alt="..." />
                            </div>
                            <div class="number">
                                <h3>3</h3>
                            </div>
                            <div class="image_3">
                                <img src="images/vector11.png" alt="...">
                            </div>
                            <div class="inner_area match_height_col">
                                <p>Professional conduct with honesty and integrity</p>
                            </div>
                            <div class="image_4">
                                <img src="images/vector12.png" alt="...">
                            </div>
                        </div>
                        <div class="content_box color_change">
                            <div class="image_1">
                                <img src="images/vector14.png" alt="..." />
                            </div>
                            <div class="image_2">
                                <img src="images/vector13.png" alt="..." />
                            </div>
                            <div class="number">
                                <h3>4</h3>
                            </div>
                            <div class="image_3">
                                <img src="images/vector11.png" alt="...">
                            </div>
                            <div class="inner_area match_height_col">
                                <p>Professional conduct with honesty and integrity</p>
                            </div>
                            <div class="image_4">
                                <img src="images/vector12.png" alt="...">
                            </div>
                        </div>
                        <div class="content_box color_change">
                            <div class="image_1">
                                <img src="images/vector14.png" alt="..." />
                            </div>
                            <div class="image_2">
                                <img src="images/vector13.png" alt="..." />
                            </div>
                            <div class="number">
                                <h3>5</h3>
                            </div>
                            <div class="image_3">
                                <img src="images/vector11.png" alt="...">
                            </div>
                            <div class="inner_area match_height_col">
                                <p>Professional conduct with honesty and integrity</p>
                            </div>
                            <div class="image_4">
                                <img src="images/vector12.png" alt="...">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Sustainable_Energy_Section_End ==== -->


<!-- ==== footer === -->
<?php include('common/footer.php') ?>