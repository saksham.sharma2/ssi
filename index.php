<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Banner_section_start ==== -->

<section class="banner_section">
    <div class="banner_area">
        <div class="inner_area">
            <div class="owl-carousel" id="banner">
                <div class="item">
                    <div class="item_inner_box">
                        <div class="img_area">
                            <img src="images/banner1.png" alt=".." />
                        </div>
                        <div class="content_box">
                            <div class="container p-0 mx-auto">
                                 <div class="content_inner">
                                     <h2>
                                        Welcome to <br>Seven Seas Petroleum LLC
                                     </h2>
                                     <p>
                                        Engineered Solutions, Automation, Digitalisation & Instrumentation
                                     </p>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item_inner_box">
                        <div class="img_area">
                            <img src="images/banner2.png" alt=".." />
                        </div>
                        <div class="content_box">
                            <div class="container p-0 mx-auto">
                                 <div class="content_inner">
                                     <h2>
                                     Leading Service Provider to the Oil & Gas Industry
                                     </h2>
                                     <p>
                                        Engineered Solutions, Automation, Digitalisation & Instrumentation
                                     </p>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item_inner_box">
                        <div class="img_area">
                            <img src="images/banner3.png" alt=".." />
                        </div>
                        <div class="content_box">
                            <div class="container p-0 mx-auto">
                                 <div class="content_inner">
                                     <h2>
                                     Leading Service Provider to the Oil & Gas Industry
                                     </h2>
                                     <p>
                                        Engineered Solutions, Automation, Digitalisation & Instrumentation
                                     </p>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="item_inner_box">
                        <div class="img_area">
                            <img src="images/banner4.png" alt=".." />
                        </div>
                        <div class="content_box">
                            <div class="container p-0 mx-auto">
                                 <div class="content_inner">
                                     <h2>
                                     Leading Service Provider to the Oil & Gas Industry
                                     </h2>
                                     <p>
                                        Engineered Solutions, Automation, Digitalisation & Instrumentation
                                     </p>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Banner_section_end ==== -->

<!-- ==== About_section_start ==== -->
<section class="about_section">
    <div class="box_area">
        <div class="img_area">
            <img src="images/Asset5.png" alt="..." />
        </div>
        <div class="content">
            <div class="tab_links">
                <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <button class="nav-link active" id="v-pills-about-tab" data-bs-toggle="pill" data-bs-target="#v-pills-about" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true">
                        <div class="icon_area">
                            <div class="left">
                                <i class="far fa-check"></i>
                            </div>
                            <div class="right">
                                <h5>ABOUT US</h5><p>Company overview</p>
                            </div>
                        </div>
                    </button>
                    <button class="nav-link" id="v-pills-history-tab" data-bs-toggle="pill" data-bs-target="#v-pills-history" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false">
                        <div class="icon_area">
                            <div class="left">
                                <i class="far fa-check"></i>
                            </div>
                            <div class="right">
                                <h5>OUR HISTORY</h5><p>Defining Milestone</p>
                            </div>
                        </div>
                    </button>
                    <button class="nav-link" id="v-pills-strengths-tab" data-bs-toggle="pill" data-bs-target="#v-pills-strengths" type="button" role="tab" aria-controls="v-pills-messages" aria-selected="false">
                        <div class="icon_area">
                            <div class="left">
                                <i class="far fa-check"></i>
                            </div>
                            <div class="right">
                                <h5>OUR STRENGTHS</h5><p>20+ Years of Experience</p>
                            </div>
                        </div>
                    </button>
                    <button class="nav-link" id="v-pills-vision-tab" data-bs-toggle="pill" data-bs-target="#v-pills-vision" type="button" role="tab" aria-controls="v-pills-settings" aria-selected="false">
                        <div class="icon_area">
                            <div class="left">
                                <i class="far fa-check"></i>
                            </div>
                            <div class="right">
                                <h5>VISION & MISSION</h5><p>Company Overview</p>
                            </div>
                        </div>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
            </div>
            <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                <div class="right_area">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-about" role="tabpanel" aria-labelledby="v-pills-about-tab">
                            <div class="heading_area">
                                <h4>Welcome to Seven Seas Petroleum LLC</h4>
                                <h2>ABOUT US</h2>
                            </div>
                            <div class="content">
                                <P>Seven Seas Petroleum LLC provides vital services to the Oman oil & gas industry, assisting local and 
                                    multinational oil companies to enhance, maintain and successfully manage their businesses in Oman.
                                </p>
                                <p>
                                    Seven Seas Petroleum prides itself in its consistent application of the newest available technology
                                    and providing an especially high quality of service. This allows the company to support and partner
                                    manufacturing and service businesses to thrive in the challenging and ever-changing market. Seven 
                                    Seas Petroleum continues to provide invaluable insight and expertise that enable client companies 
                                    to maximize hydrocarbon recovery as it focuses on Oil and Gas fields that present increasing technical challenges.
                                </p>
                                <p>    
                                    Seven Seas Petroleum has successfully diversified its business by extending its services to different
                                    sectors like Power & Energy Petrochemical, Water & Waste Water Engineering & Contracting and other 
                                    related industries.
                                </P>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-history" role="tabpanel" aria-labelledby="v-pills-history-tab">
                            <div class="heading_area">
                                <h4>Welcome to Seven Seas Petroleum LLC</h4>
                                <h2>OUR HISTORY</h2>
                            </div>
                            <div class="content">
                                <P>Seven Seas Petroleum LLC provides vital services to the Oman oil & gas industry, assisting local and 
                                    multinational oil companies to enhance, maintain and successfully manage their businesses in Oman.
                                </p>
                                <p>
                                    Seven Seas Petroleum prides itself in its consistent application of the newest available technology
                                    and providing an especially high quality of service. This allows the company to support and partner
                                    manufacturing and service businesses to thrive in the challenging and ever-changing market. Seven 
                                    Seas Petroleum continues to provide invaluable insight and expertise that enable client companies 
                                    to maximize hydrocarbon recovery as it focuses on Oil and Gas fields that present increasing technical challenges.
                                </p>
                                <p>    
                                    Seven Seas Petroleum has successfully diversified its business by extending its services to different
                                    sectors like Power & Energy Petrochemical, Water & Waste Water Engineering & Contracting and other 
                                    related industries.
                                </P>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-strengths" role="tabpanel" aria-labelledby="v-pills-strengths-tab">
                            <div class="heading_area">
                                <h4>Welcome to Seven Seas Petroleum LLC</h4>
                                <h2>OUR STRENGTHS</h2>
                            </div>
                            <div class="content">
                                <P>Seven Seas Petroleum LLC provides vital services to the Oman oil & gas industry, assisting local and 
                                    multinational oil companies to enhance, maintain and successfully manage their businesses in Oman.
                                </p>
                                <p>
                                    Seven Seas Petroleum prides itself in its consistent application of the newest available technology
                                    and providing an especially high quality of service. This allows the company to support and partner
                                    manufacturing and service businesses to thrive in the challenging and ever-changing market. Seven 
                                    Seas Petroleum continues to provide invaluable insight and expertise that enable client companies 
                                    to maximize hydrocarbon recovery as it focuses on Oil and Gas fields that present increasing technical challenges.
                                </p>
                                <p>    
                                    Seven Seas Petroleum has successfully diversified its business by extending its services to different
                                    sectors like Power & Energy Petrochemical, Water & Waste Water Engineering & Contracting and other 
                                    related industries.
                                </P>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-vision" role="tabpanel" aria-labelledby="v-pills-vision-tab">
                            <div class="heading_area">
                                <h4>Welcome to Seven Seas Petroleum LLC</h4>
                                <h2>VISION & MISSION</h2>
                            </div>
                            <div class="content">
                                <P>Seven Seas Petroleum LLC provides vital services to the Oman oil & gas industry, assisting local and 
                                    multinational oil companies to enhance, maintain and successfully manage their businesses in Oman.
                                </p>
                                <p>
                                    Seven Seas Petroleum prides itself in its consistent application of the newest available technology
                                    and providing an especially high quality of service. This allows the company to support and partner
                                    manufacturing and service businesses to thrive in the challenging and ever-changing market. Seven 
                                    Seas Petroleum continues to provide invaluable insight and expertise that enable client companies 
                                    to maximize hydrocarbon recovery as it focuses on Oil and Gas fields that present increasing technical challenges.
                                </p>
                                <p>    
                                    Seven Seas Petroleum has successfully diversified its business by extending its services to different
                                    sectors like Power & Energy Petrochemical, Water & Waste Water Engineering & Contracting and other 
                                    related industries.
                                </P>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== About_section_end ==== -->

<!-- ==== Chairman_section_start ==== -->
<section class="chairman_message_section">
    <div class="wraper">
    </div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-5 col-sm-12 col-12">
            <div class="image_area">
        <img src="images/chairman.png" alt="..." />
    </div>
            </div>
            <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-7 col-sm-12 col-12">
                <div class="content">
                    <div class="inner_area">
                        <h4>
                            CHAIRMAN’S MESSAGE
                        </h4>
                        <p>As a local leader in petroleum services, we have a talented 
                            management team to develop and manage businesses through our knowledge, 
                            experience and global network of relationships. With oilfield equipment 
                            and services in high demand, Seven Seas Petroleum is well placed to provide 
                            high-quality and cost-effective services to the national and International Oil 
                            & Gas companies in Oman. Ultimately, we are driven by our strong sense of values. 
                            We form close partnerships with the companies we support and invest in, and we 
                            build strong relationships based on Integrity, trust and mutual respect.
                        </p>
                        <h6>
                            H.H. Seyyid Shihab bin Tariq Al Said Chairman
                        </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Chairman_section_end ==== -->

<!-- ==== Our_Business_section_start ==== -->
<section class="our_business_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="header">
                    <div class="header_area">
                        <p>
                            What we do for you
                        </p>
                        <a href="javascript:;" class="busniess">
                            <h4>
                                OUR BUSINESS
                            </h4>
                        </a>
                        <a href="javascript:;" class="serv">
                            <h6>
                                OUR SERVICES
                            </h6>
                        </a>
                    </div>
                    </div>
                </div>
                <div class="review_slider">
                    <div class="review_set">
                        <div class="owl-carousel" id="our_business">
                            <div class="item">
                                <div class="review_inner_box">
                                    <div class="content_area">
                                        <div class="left_area">
                                            <div class="inner_area">
                                                <div class="right d-lg-none d-block">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="left">
                                                    <h5>Control and safety systems</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                                <div class="right d-lg-block d-none">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="right_area">
                                            <div class="inner_area">
                                                <div class="left">
                                                    <div class="image">
                                                        <img src="images/business2.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="right">
                                                    <h5>Asset management</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_inner_box">
                                    <div class="content_area">
                                        <!-- <div class="left_area">
                                            <div class="inner_area">
                                                <div class="right d-lg-none d-block">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="left">
                                                    <h5>Control and safety systems</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                                <div class="right d-lg-block d-none">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="right_area">
                                            <div class="inner_area">
                                                <div class="left">
                                                    <div class="image">
                                                        <img src="images/business2.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="right">
                                                    <h5>Asset management</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_inner_box">
                                    <div class="content_area">
                                        <div class="left_area">
                                            <div class="inner_area">
                                                <div class="right d-lg-none d-block">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="left">
                                                    <h5>Control and safety systems</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                                <div class="right d-lg-block d-none">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="right_area">
                                            <div class="inner_area">
                                                <div class="left">
                                                    <div class="image">
                                                        <img src="images/business2.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="right">
                                                    <h5>Asset management</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_inner_box">
                                    <div class="content_area">
                                        <!-- <div class="left_area">
                                            <div class="inner_area">
                                                <div class="right d-lg-none d-block">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="left">
                                                    <h5>Control and safety systems</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                                <div class="right d-lg-block d-none">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="right_area">
                                            <div class="inner_area">
                                                <div class="left">
                                                    <div class="image">
                                                        <img src="images/business2.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="right">
                                                    <h5>Asset management</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="review_slider services">
                    <div class="review_setes d-none">
                        <div class="owl-carousel" id="our_services">
                            <div class="item">
                                <div class="review_inner_box">
                                    <div class="content_area">
                                        <div class="left_area">
                                            <div class="inner_area">
                                                <div class="right d-lg-none d-block">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="left">
                                                    <h5>Control and safety systems</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                                <div class="right d-lg-block d-none">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="right_area">
                                            <div class="inner_area">
                                                <div class="left">
                                                    <div class="image">
                                                        <img src="images/business2.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="right">
                                                    <h5>Asset management</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_inner_box">
                                    <div class="content_area">
                                        <!-- <div class="left_area">
                                            <div class="inner_area">
                                                <div class="right d-lg-none d-block">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="left">
                                                    <h5>Control and safety systems</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                                <div class="right d-lg-block d-none">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="right_area">
                                            <div class="inner_area">
                                                <div class="left">
                                                    <div class="image">
                                                        <img src="images/business2.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="right">
                                                    <h5>Asset management</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_inner_box">
                                    <div class="content_area">
                                        <div class="left_area">
                                            <div class="inner_area">
                                                <div class="right d-lg-none d-block">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="left">
                                                    <h5>Control and safety systems</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                                <div class="right d-lg-block d-none">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="right_area">
                                            <div class="inner_area">
                                                <div class="left">
                                                    <div class="image">
                                                        <img src="images/business2.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="right">
                                                    <h5>Asset management</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="review_inner_box">
                                    <div class="content_area">
                                        <!-- <div class="left_area">
                                            <div class="inner_area">
                                                <div class="right d-lg-none d-block">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="left">
                                                    <h5>Control and safety systems</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                                <div class="right d-lg-block d-none">
                                                    <div class="image">
                                                        <img src="images/business1.png" alt="..." />
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="right_area">
                                            <div class="inner_area">
                                                <div class="left">
                                                    <div class="image">
                                                        <img src="images/business2.png" alt="..." />
                                                    </div>
                                                </div>
                                                <div class="right">
                                                    <h5>Asset management</h5>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin. 
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas 
                                                        gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                    </p>
                                                    <div class="button">
                                                        <a href="javascript:;" class="btn btn-primary">Read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== Our_Business_section_end ==== -->

<!-- ==== footer === -->


<?php include('common/footer.php') ?>