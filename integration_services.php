<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame_section_start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area">
                    <h2>INTEGRATION SERVICES</h2> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame_section_end ==== -->
<section class="intergrated_parent">
    <!-- ==== Integrated_metering_section_Start ==== -->
    <div class="integrated_metering_section space">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="inner_area">
                        <div class="row align-items-center">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="image_area">
                                    <img src="images/integ1.png" alt="..." />
                                </div>
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="content_area">
                                    <div class="header_area">
                                        <h4>Integrated Metering SKIDS</h4>
                                    </div>
                                    <div class="content">
                                        <p>Seas Petroleum LLC combines Emerson's customized engineering capabilities, 
                                            reliable products, best-in-class consulting and project management with industry 
                                            expertise and life-cycle services to provide innovative, integrated custody transfer 
                                            measurement systems for both liquid and gas measurement. From the simplest single-stream 
                                            skid to complex on-site installations, metering systems are manufactured to global product 
                                            and fiscal metering standards such as CE, UL, ATEX, NACE, ANSI, ASTM, AGA, ISO. These solutions 
                                            typically include meters, valves, provers, control panels, user interfaces, software and hardware, 
                                            and are seamlessly integrated to maximize measurement accuracy and minimize risk.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==== Integrated_metering_section_end ==== -->

    <!-- ==== Integrated_MSV_section_Start ==== -->
    <div class="integrated_metering_section msv">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                    <div class="inner_area">
                        <div class="row align-items-center">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12  order-lg-0 order-1">
                                <div class="content_area align-right">
                                    <div class="header_area">
                                        <h4>Integrated MSV SKIDS</h4>
                                    </div>
                                    <div class="content">
                                        <p>Seven Seas Petroleum LLC utilizes the world's leading Multiport Selector (MSV) 
                                            from Emerson - Bettis Valve Automation combined with other leading Emerson products 
                                            like Micro Motion Coriolis meter, Rosemount Pressure/Temperature Transmitters and 
                                            Digital Net Oil Computer (DNOC) to design and fabricate a fully integrated MSV skid. 
                                            The Integrated MSV skid can be utilized for remotely controlled Automated Well testing. 
                                            SSP takes single point responsibility for the design, engineering fabrication and supply 
                                            of the entire skid. The testing of the integrated skid is done in SSP workshop before it 
                                            is dispatched to site, saving valuable commissioning time at the site. SSP has supplied 
                                            over 40 such skids to various customers like PDO, Oxy Oman, Medco and Petrogas Rima.
                                        </p>
                                    </div>
                                </div>   
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                                <div class="image_area">
                                    <img src="images/integ2.png" alt="..." />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==== Integrated_MSV_section_end ==== -->

    <!-- ==== Digital_Net_oil_section_Start ==== -->
    <div class="integrated_metering_section">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="inner_area last_inner_area">
                        <div class="row align-items-center">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="image_area">
                                    <img src="images/integ1.png" alt="..." />
                                </div>
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="content_area">
                                    <div class="header_area">
                                        <h4>Digital Net Oil Computer (DNOC)</h4>
                                    </div>
                                    <div class="content">
                                        <p>We want all of our customers to experience the impressive level of 
                                            professionalism when working with Seven Seas Petroleum LLC. All of our services, 
                                            especially this one, exist to make your life easier and stress free. You can 
                                            trust us to supply you with the best products, as well as top quality customer service.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==== Digital_net_oil_section_end ==== -->
</section>

<!-- ==== footer === -->
<?php include('common/footer.php') ?>