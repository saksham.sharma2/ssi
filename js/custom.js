if($('#banner').length)
{
$('#banner').owlCarousel({
    loop:true,
    nav:true,
    navText: ["<i class='fal fa-chevron-left'></i>","<i class='fal fa-chevron-right'></i>"],
    dots:true,
    responsive:{
        0:{
            items:1,
            nav: false,
        },
        600:{
            items:1,
            nav: false,
        },
        1000:{
            items:1,
            nav: false,
        },
        1200:{
            items:1
        }
    }
})
}


var width = $(document).width();
if(width <= 768 )
{
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if(scroll >= 1) {
            $(".header-main-section ").css({'position':'fixed', 'top':'0'});
        }
        else
        {
            $(".header-main-section ").css({'position':'relative','top':'unset'});
        }
    });
}
else
{
    $(window).scroll(function () {
        if($(this).scrollTop() >= 108){
            $(".header-main-section ").css({'position':'fixed', 'top':'0'});
        } else {
            $(".header-main-section ").css({'position':'relative','top':'unset'});
        }
    });
}

// Height match columns
$(document).ready(function () {
    $('.match_height_col').matchHeight();
    $('.match_height_txt').matchHeight();
});


if($('#our_business').length)
{
$('#our_business').owlCarousel({
    loop:true,
    margin:50,
    items:2,
    slideBy: 2,
    stagePadding: 15,
    nav:true,
    navText: ["<i class='fal fa-chevron-left'></i>","<i class='fal fa-chevron-right'></i>"],
    dots:false,
   
    responsive:{
        0:{
            items:1,
            slideBy: 1,
        },
        600:{
            items:2,
            // slideBy: 1,
        },
        1000:{
            items:2,
        },
        1200:{
            items:2
        }
    }
})
}

if($('#our_services').length)
{
$('#our_services').owlCarousel({
    loop:true,
    margin:50,
    items:2,
    slideBy: 2,
    stagePadding: 15,
    nav:true,
    navText: ["<i class='fal fa-chevron-left'></i>","<i class='fal fa-chevron-right'></i>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            slideBy: 1,
        },
        600:{
            items:2,
        },
        1000:{
            items:2,
        },
        1200:{
            items:2,
        }
    }
})
}

if($('#project').length)
{
$('#project').owlCarousel({
    loop:true,
    margin:50,
    items:3,
    //slideBy: 1,
    stagePadding: 15,
    nav:true,
    navText: ["<i class='fal fa-chevron-left'></i>","<i class='fal fa-chevron-right'></i>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
        },
        600:{
            items:2,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
            // slideBy: 1,
        },
        1000:{
            items:2,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
        },
        1200:{
            items:3,
        }
    }
})
}

if($('#our_team').length)
{
$('#our_team').owlCarousel({
    loop:true,
    margin:50,
    items:4,
    //slideBy: 1,
    stagePadding: 15,
    nav:true,
    navText: ["<i class='fal fa-chevron-left'></i>","<i class='fal fa-chevron-right'></i>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            nav:false,
             autoplay:true,
             autoplayTimeout:2500,
        },
        600:{
            items:2,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
            // slideBy: 1,
        },
        1000:{
            items:3,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
        },
        1200:{
            items:4,
        }
    }
})
}

if($('#testimonials').length)
{
$('#testimonials').owlCarousel({
    center: true,
    loop:true,
    margin:50,
    items:3,
    //slideBy: 1,
    stagePadding: 15,
    nav:true,
    navText: ["<i class='fal fa-chevron-left'></i>","<i class='fal fa-chevron-right'></i>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            nav:false,
            //  autoplay:true,
            //  autoplayTimeout:2500,
        },
        600:{
            items:2,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
            // slideBy: 1,
        },
        1000:{
            items:3,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
        },
        1200:{
            items:3,
        }
    }
})
}

if($('#system_services').length)
{
$('#system_services').owlCarousel({
    loop:true,
    margin:50,
    items:4,
    //slideBy: 1,
    stagePadding: 15,
    nav:true,
    navText: ["<i class='fal fa-chevron-left'></i>","<i class='fal fa-chevron-right'></i>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            nav:false,
             autoplay:true,
             autoplayTimeout:2500,
        },
        600:{
            items:2,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
        },
        1000:{
            items:3,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
        },
        1200:{
            items:4,
        }
    }
})
}

if($('#human_resources').length)
{
$('#human_resources').owlCarousel({
    loop:true,
    margin:50,
    items:4,
    //slideBy: 1,
    stagePadding: 15,
    nav:true,
    navText: ["<i class='fal fa-chevron-left'></i>","<i class='fal fa-chevron-right'></i>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
        },
        600:{
            items:2,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
           
        },
        1000:{
            items:3,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
        },
        1200:{
            items:4,
        }
    }
})
}

if($('#mechanical_services').length)
{
$('#mechanical_services').owlCarousel({
    loop:true,
    margin:50,
    items:4,
    //slideBy: 1,
    stagePadding: 15,
    nav:true,
    navText: ["<i class='fal fa-chevron-left'></i>","<i class='fal fa-chevron-right'></i>"],
    dots:false,
    responsive:{
        0:{
            items:1,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
        },
        600:{
            items:2,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
        },
        1000:{
            items:3,
            nav:false,
            autoplay:true,
            autoplayTimeout:2500,
        },
        1200:{
            items:4,
        }
    }
})
}

$('body').on('click', '.busniess', function(){
    $(this).parents('.our_business_section').find('.review_set').removeClass('d-none');
    $(this).parents('.our_business_section').find('.review_setes').addClass('d-none');
    $(this).parents('.our_business_section').find('.busniess').removeClass('two');
    $(this).parents('.our_business_section').find('.serv').removeClass('one');
    
});

$('body').on('click', '.serv', function(){
    $(this).parents('.our_business_section').find('.review_set').addClass('d-none');
    $(this).parents('.our_business_section').find('.review_setes').removeClass('d-none');
    $(this).parents('.our_business_section').find('.serv').addClass('one');
    $(this).parents('.our_business_section').find('.busniess').addClass('two');
    
});




// Open Close resposnive menu
/* Open menu */
if($('.res_menubar').length)
{
$('body').on('click', '.res_menubar', function(){
    $(this).parents('.responsive_menu').find('.bar').addClass('hide_bar');
    $(this).parents('.responsive_menu').find('.open_menu').addClass('show_menu');
    $('body').addClass('scroll_off');
});
}

// Open Close resposnive menu
/* Open menu */
if($('.res_menubar').length)
{
$('body').on('click', '.res_menubar', function(){
    $(this).parents('.responsive_menu').find('.open_menu').addClass('show_open');
    $('body').addClass('scroll_off');
});
}
/* Close menu */
if($('.cross_menu a').length)
{
$('body').on('click', '.cross_menu a', function(){
    $(this).parents('.responsive_menu').find('.open_menu.show_open').removeClass('show_open');
    $('body').removeClass('scroll_off');
});
}


// Open Close resposnive menu
/* Open menu */


if($('.serch_open').length)
{
$('body').on('click', '.serch_open', function(){
    that = $(this);
    parent = that.parents('.header_menu_wraper');
    parent.find('.for_secrh').toggleClass('add');
    parent.find('.right').toggleClass('add');
});
}

if($('.open').length)
{
$('body').on('click', '.opened', function(){
    that = $(this);
    parent = that.parents('.search_box');
    parent.find('.form-control').toggleClass('open');
});
}


// if($('.serch_close').length)
// {
// $('body').on('click', '.serch_close', function(){
//     that = $(this);
//     parent = that.parents('.header_menu_wraper');
//     parent.find('.open').removeClass('d-none');
//     parent.find('.close').addClass('d-none');
//     parent.find('.right').css("display", "none");
// });
// }





document.addEventListener("DOMContentLoaded", function(){
    // make it as accordion for smaller screens
    if (window.innerWidth < 992) {
    
      // close all inner dropdowns when parent is closed
      document.querySelectorAll('.navbar .dropdown').forEach(function(everydropdown){
        everydropdown.addEventListener('hidden.bs.dropdown', function () {
          // after dropdown is hidden, then find all submenus
            this.querySelectorAll('.submenu').forEach(function(everysubmenu){
              // hide every submenu as well
              everysubmenu.style.display = 'none';
            });
        })
      });
    
      document.querySelectorAll('.dropdown-menu a').forEach(function(element){
        element.addEventListener('click', function (e) {
            let nextEl = this.nextElementSibling;
            if(nextEl && nextEl.classList.contains('submenu')) {	
              // prevent opening link if link needs to open dropdown
              e.preventDefault();
              if(nextEl.style.display == 'block'){
                nextEl.style.display = 'none';
              } else {
                nextEl.style.display = 'block';
              }
    
            }
        });
      })
    }
    // end if innerWidth
    }); 
    // DOMContentLoaded  end

const Shuffle = window.Shuffle;
const element = document.querySelector('.my-shuffle-container');
const sizer = element.querySelector('.my-sizer-element');

const shuffleInstance = new Shuffle(element, {
  itemSelector: '.picture-item',
  sizer: sizer // could also be a selector: '.my-sizer-element'
});