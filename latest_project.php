<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== banner_section_start ==== -->
<section class="banner_section_other" style="background-image: url('./images/layer9.png')">
    <div class="banner_area">
        <div class="inner_area">
            <p>Latest Project</p>
        </div>
    </div>
</section>
<!-- ==== banner_section_end ==== -->

<!-- ==== service_section1_start ==== -->
<section class="service_section pd_top">
    <div class="box_area">
        <div class="images">
            <img src="images/layer2.png">
        </div>
    </div>
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-1">
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="content_area">
                        <div class="heading">
                            <h3>Project 1</h3>
                        </div>
                        <div class="content">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam 
                                nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat 
                                volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation 
                                ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie 
                                consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan 
                                et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis 
                                dolore te feugait nulla facilisi. 
                            </p>
                            <p>Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod 
                                tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, 
                                quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo 
                                consequat.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== service_section1_end ==== -->

<!-- ==== service_section2_start ==== -->
<section class="service_section">
    <div class="box_area el">
        <div class="images">
            <img src="images/layer3.png">
        </div>
    </div>
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">  
                    <div class="content_area  left_side">
                        <div class="heading">
                            <h3>Project 2</h3>
                        </div>
                        <div class="content">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam 
                                nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat 
                                volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation 
                                ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie 
                                consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan 
                                et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis 
                                dolore te feugait nulla facilisi. 
                            </p>
                            <p>Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod 
                                tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, 
                                quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo 
                                consequat.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                   
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== service_section2_end ==== -->

<!-- ==== service_section3_start ==== -->
<section class="service_section">
    <div class="box_area">
        <div class="images">
            <img src="images/layer4.png">
        </div>
    </div>
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="content_area instru_padding">
                        <div class="heading">
                            <h3>Project 3</h3>
                        </div>
                        <div class="content">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam 
                                nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat 
                                volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation 
                                ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie 
                                consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan 
                                et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis 
                                dolore te feugait nulla facilisi. 
                            </p>
                            <p>Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod 
                                tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, 
                                quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo 
                                consequat.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== service_section3_end ==== -->

<!-- ==== service_section4_start ==== -->
<section class="service_section">
    <div class="box_area el">
        <div class="images">
            <img src="images/layer7.png">
        </div>
    </div>
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="content_area left_side field_service_padding">
                        <div class="heading">
                            <h3>Project 4</h3>
                        </div>
                        <div class="content">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam 
                                nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat 
                                volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation 
                                ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie 
                                consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan 
                                et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis 
                                dolore te feugait nulla facilisi. 
                            </p>
                            <p>Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod 
                                tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, 
                                quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo 
                                consequat.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                   
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== service_section4_end ==== -->

<!-- ==== service_section5_start ==== -->
<section class="service_section">
    <div class="box_area">
        <div class="images">
        <img src="images/layer6.png">
        </div>
    </div>
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="content_area turnkey_padding">
                        <div class="heading">
                            <h3>Project 5</h3>
                        </div>
                        <div class="content">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam 
                                nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat 
                                volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation 
                                ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie 
                                consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan 
                                et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis 
                                dolore te feugait nulla facilisi. 
                            </p>
                            <p>Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod 
                                tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, 
                                quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo 
                                consequat.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== service_section5_end ==== -->

<!-- ==== service_section6_start ==== -->
<section class="service_section">
    <div class="box_area el">
        <div class="images">
            <img src="images/layer5.png">
        </div>
    </div>
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                    <div class="content_area left_side mechanical_padding">
                        <div class="heading">
                            <h3>Project 6</h3>
                        </div>
                        <div class="content">
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam 
                                nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat 
                                volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation 
                                ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. 
                                Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie 
                                consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan 
                                et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis 
                                dolore te feugait nulla facilisi. 
                            </p>
                            <p>Lorem ipsum dolor sit amet, cons ectetuer adipiscing elit, sed diam nonummy nibh euismod 
                                tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, 
                                quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo 
                                consequat.
                            </p>
                            <p>
                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                   
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== service_section6_end ==== -->

<!-- ==== footer === -->
<?php include('common/partner.php') ?>
<?php include('common/get_in_touch.php') ?>
<?php include('common/footer.php') ?>