<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame_section_start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area">
                    <h2>MECHANICAL SEAL REFURBISHMENT CENTER</h2> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame_section_end ==== -->

<section class="intergrated_parent">
    <!-- ==== John_crane_facility_section_Start ==== -->
    <div class="integrated_metering_section">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="inner_area mechanical_seal_inner_area">
                        <div class="row align-items-center">
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="image_area">
                                    <img src="images/integ1.png" alt="..." />
                                </div>
                            </div>
                            <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                <div class="content_area">
                                    <div class="header_area">
                                        <h4>John Crane Facility At Our VAC</h4>
                                    </div>
                                    <div class="content">
                                        <p>The facility aims to enhance local services capability within Oman and ICV
                                            (In-Country Value), along with quick turnaround of repairs/refurbishment for our
                                            clients in the region. This facility provides a platform to develop technical skills  
                                            & experience of young Omani Engineers/Technicians in Mechanical seals services & maintenance. 
                                            Moving further, it is intended to increase job opportunities
                                            for Omani nationals with increasing number of repair contracts/agreements with local companies. 
                                            The approach will save considerable time and money for our customers as Mechanical seals will  
                                            be now be repaired within Oman without crossing borders.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ==== John_crane_facility_section_end ==== -->

    <!-- ==== John_crane_Service_section_Start ==== -->
    <div class="crane_service_section">
        <div class="container">
            <div class="row">
                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="content_area">
                        <div class="header">
                            <h3>John Crane Service Capabilities</h3>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="content_box">
                                    <div class="inner_area match_height_col">
                                        <p>Fully Equipped Wet Mechanical Seal Service Center</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="content_box">
                                    <div class="inner_area match_height_col">
                                        <p>Repair, Refurbishment and Retrofitting of Mechanical Seals</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="content_box">
                                    <div class="inner_area match_height_col">
                                        <p>Comprehensive service provider with dedicated team of Service Engineer and technician</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="content_box">
                                    <div class="inner_area match_height_col">
                                        <p>Site service activities include commissioning, trouble shooting and reliability</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ==== John_crane_Service_section_end ==== -->

<!-- ==== footer === -->
<?php include('common/footer.php') ?>