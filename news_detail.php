<!-- ==== Header === -->
<?php include('common/header.php') ?>


<!-- ==== Breadcame_Section_Start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area">
                    <h2>NEWS</h2> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame_Section_End ==== -->

<!-- ==== Latest_news_Section_Start ==== -->
<section class="latest_news_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_area">
                    <div class="row">
                        <div class="col-xxl-8 col-xl-8 col-lg-8 col-md-8 col-sm-8 col-12">
                            <div class="left_area">
                                <div class="image">
                                    <img src="images/news_img.png" alt="..." />
                                </div>
                                <div class="header">
                                    <p>6jun,2022</p>
                                    <h3>Lorem ipsum sit dor amrt</h3>                        
                                </div>
                                <div class="content">
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur</p>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur</p>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
                                </div>
                                <div class="social_sites">
                                    <ul>
                                        <li><p>Share</p></li>
                                        <li><a href="javascript:;"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="javascript:;"><i class="fab fa-google-plus-g"></i></a></li>
                                        <li><a href="javascript:;"><i class="fab fa-linkedin-in"></i></a></li>
                                        <li><a href="javascript:;"><i class="fab fa-pinterest"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
                            <div class="right_area">
                                <div class="header">
                                    <h4>Latest News</h4>
                                </div>
                                <div class="content_box">
                                    <div class="inner_area">
                                        <h6>6jun,2022</h6>
                                        <h4>Lorem ipsum sit dor amet dor</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit purus sit amet...<a href="news_detail.php">Read More</a></p>
                                    </div>
                                    <div class="inner_area">
                                        <h6>6jun,2022</h6>
                                        <h4>Lorem ipsum sit dor amet dor</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit purus sit amet...<a href="news_detail.php">Read More</a></p>
                                    </div>
                                    <div class="inner_area">
                                        <h6>6jun,2022</h6>
                                        <h4>Lorem ipsum sit dor amet dor</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit purus sit amet...<a href="news_detail.php">Read More</a></p>
                                    </div>
                                    <div class="inner_area border_remove">
                                        <h6>6jun,2022</h6>
                                        <h4>Lorem ipsum sit dor amet dor</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit purus sit amet...<a href="news_detail.php">Read More</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Latest_news_Section_End ==== -->
<!-- ==== Related_news_Section_Start ==== -->
<section class="related_project_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="contents_area">
                    <div class="header">
                        <h4>Related News</h4>
                    </div>
                    <div class="project_slider">
                        <div class="review_set">
                            <div class="owl-carousel" id="project">
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/project1.png" alt="..." />
                                        </div>
                                        <div class="content news_opacity">
                                            <div class="inner_area">
                                                <h4> Project 1</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                    purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                </p>
                                                <a href="news_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/project2.png" alt="..." />
                                        </div>
                                        <div class="content news_opacity">
                                            <div class="inner_area">
                                                <h4> Project 2</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                    purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                </p>
                                                <a href="news_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/project3.png" alt="..." />
                                        </div>
                                        <div class="content news_opacity">
                                            <div class="inner_area">
                                                <h4> Project 3</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                    purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                </p>
                                                <a href="news_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/project4.png" alt="..." />
                                        </div>
                                        <div class="content news_opacity">
                                            <div class="inner_area">
                                                <h4> Project 4</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                    purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                </p>
                                                <a href="news_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/project5.png" alt="..." />
                                        </div>
                                        <div class="content news_opacity">
                                            <div class="inner_area">
                                                <h4> Project 5</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                    purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                </p>
                                                <a href="news_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Related_news_Section_End ==== -->



<!-- ==== footer === -->
<?php include('common/footer.php') ?>