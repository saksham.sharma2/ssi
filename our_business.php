<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame_section_start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area">
                    <h2>OUR BUSINESS</h2> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame_section_end ==== -->

<!-- ==== Parent section_Start ==== -->
<section class="intergrated_parent">
<!-- ==== Instrumentation and Automation Solutions_section_Start ==== -->
<div class="integrated_metering_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="inner_area mechanical_seal_inner_area">
                    <div class="row align-items-center">
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="image_area">
                                <img src="images/about1.png" alt="..." />
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="content_area">
                                <div class="header_area">
                                    <h4>Instrumentation and Automation Solutions</h4>
                                </div>
                                <div class="content">
                                    <p>Seven Seas Petroleum operates an Emerson Certified Service Facility in Muscat for Process Automation Systems (ICSS).  
                                        All Priority spares are stocked at this facility   for immediate availability. All the engineers are at the current level of 
                                        certification for DeltaV, DeltaV SIS, and have extensive exposure to the Delta V environment in Oman. Seven Seas Petroleum team has successfully  
                                        commissioned over  50+ systems  at  various customer locations including some very large sites at PDO Rabab Harweel and PDO Qarn Alam 
                                        Steam EOR which were projects of national importance.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ==== Instrumentation and Automation Solutions_Section_End ==== -->

<!-- ==== Business_Detail_Section_Start ==== -->
<div class="business_detail_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_box">
                    <div class="row">
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="row">
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="business_detail_area">
                                        <div class="right d-lg-none d-block">
                                            <div class="image_area">
                                                <img src="images/about3.png" alt="..." />
                                            </div>
                                        </div>
                                        <div class="left">
                                            <div class="header">
                                                <h2>Control and safety systems</h2>
                                            </div>
                                            <div class="content">
                                                <p>A complete system offering -from Distributed Control Systems to Safety Instrumented Systems, 
                                                Emerson's  superior technology combined  with industry-specific engineering,  consulting, 
                                                project management and  maintenance services that  facilitate operations in an easy, intuitive, and 
                                                interoperable way.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="right d-lg-block d-none">
                                            <div class="image_area">
                                                <img src="images/about3.png" alt="..." />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="business_detail_area">
                                        <div class="right d-lg-none d-block">
                                            <div class="image_area">
                                                <img src="images/about3.png" alt="..." />
                                            </div>
                                        </div>
                                        <div class="left">
                                            <div class="header">
                                                <h2>Measurement solutions</h2>
                                            </div>
                                            <div class="content">
                                                <p>An Unparalleled breadth of Field Instruments  and  Analyzers, including Pressure, Temperature, 
                                                    Level and Flow Instruments, Tank Gauging Systems, Liquid and  Gas Analysis, GCs, Flame and Gas Detection, 
                                                    Wireless Field Instruments.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="right d-lg-block d-none">
                                            <div class="image_area">
                                                <img src="images/about3.png" alt="..." />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="business_detail_area">
                                        <div class="right d-lg-none d-block">
                                            <div class="image_area">
                                                <img src="images/about3.png" alt="..." />
                                            </div>
                                        </div>
                                        <div class="left">
                                            <div class="header">
                                                <h2>Solenoids and pneumatics</h2>
                                            </div>
                                            <div class="content">
                                                <p>industry-leading products (ASCO) that  operate at low-power and with all global approvals 
                                                    to maximize customer efficiencies and generate measurable performance improvements.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="right d-lg-block d-none">
                                            <div class="image_area">
                                                <img src="images/about3.png" alt="..." />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6 col-xl-6 col-lg-6 col-lg-6 col-md-12 col-sm-12 col-12">
                            <div class="row top_spc">
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="business_detail_area">
                                        <div class="right right_space">
                                            <div class="image_area">
                                                <img src="images/business_img.png" alt="..." />
                                            </div>
                                        </div>
                                        <div class="left left_align">
                                            <div class="header">
                                                <h2>Asset management</h2>
                                            </div>
                                            <div class="content">
                                                <p>A variety of asset management and condition indicator technologies specific 
                                                    to the nature and criticality of   production assets.  Predictive intelligence and integrated 
                                                    protection technologies can be implemented to reach business goals and achieve top-quartile reliability.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="business_detail_area">
                                        <div class="right right_space">
                                            <div class="image_area">
                                                <img src="images/business_img.png" alt="..." />
                                            </div>
                                        </div>
                                        <div class="left left_align">
                                            <div class="header">
                                                <h2>Valves, Actuators and regulators</h2>
                                            </div>
                                            <div class="content">
                                                <p>Aighly reliable control technologies to help you regulate and isolate   your process with certainty. 
                                                    Emerson Final Control Division comprises of most trusted brands and  is backed by decades of field-proven 
                                                    experience enabling customers to achieve top quartile performance.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="business_detail_area">
                                        <div class="right right_space">
                                            <div class="image_area">
                                                <img src="images/business_img.png" alt="..." />
                                            </div>
                                        </div>
                                        <div class="left left_align">
                                            <div class="header">
                                                <h2>Services and consulting</h2>
                                            </div>
                                            <div class="content">
                                                <p>Our consultants employ innovative technologies to accelerate your competitive edge. Project Services engineers 
                                                    support you from plant concept to start-up. Once your plant is running, our Lifecycle Services 
                                                    personnel offer Maintenance, Reliability and Performance services-where and when needed. Our Educational Services 
                                                    can train your staff.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ==== Business_Detail_Section_End ==== -->





</section>
<!-- ==== Parent_section_end ==== -->

<!-- ==== footer === -->
<?php include('common/footer.php') ?>