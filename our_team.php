<!-- ==== Header === -->
<?php include('common/header.php') ?>


<!-- ==== Breadcame_Section_Start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area">
                    <h2>OUR TEAM</h2> 
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame_Section_End ==== -->

<!-- ==== Banner_Section_Start ==== -->
<section class="team_banner_section" style="background-image:url('./images/banner.png')">
</section>
<!-- ==== Banner_Section_End ==== -->

<!-- ==== Field_services_Section_Start ==== -->
<section class="field_services_section top_spacee">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="contents_area">
                    <div class="header header_color">
                        <h4>Field Services</h4>
                    </div>
                    <div class="project_slider">
                        <div class="review_set">
                            <div class="owl-carousel" id="our_team">
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/team1.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/team2.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/team3.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/team4.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/team1.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Field_services_Section_End ==== -->

<!-- ==== System_services_Section_Start ==== -->
<section class="field_services_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="contents_area">
                    <div class="header">
                        <h4>System Services</h4>
                    </div>
                    <div class="project_slider">
                        <div class="review_set">
                            <div class="owl-carousel" id="system_services">
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/system1.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/system2.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/system3.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/system4.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/system1.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== System_services_Section_End ==== -->

<!-- ==== Human_resources_Section_Start ==== -->
<section class="field_services_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="contents_area">
                    <div class="header">
                        <h4>Human Resources</h4>
                    </div>
                    <div class="project_slider">
                        <div class="review_set">
                            <div class="owl-carousel" id="human_resources">
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/project1.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/human2.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/human3.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/human4.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/human1.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Human_resources_Section_End ==== -->

<!-- ==== Mechanical_services_Section_Start ==== -->
<section class="field_services_section bottom_space">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="contents_area">
                    <div class="header">
                        <h4>Mechanical Services</h4>
                    </div>
                    <div class="project_slider">
                        <div class="review_set">
                            <div class="owl-carousel" id="mechanical_services">
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/mechanical1.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/mechanical2.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/mechanical3.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/mechanical4.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/mechanical1.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4>Lorem el ipsum</h4>
                                                <p>Sitdoramet  lorem</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Mechanical_services_Section_End ==== -->



<!-- ==== footer === -->
<?php include('common/footer.php') ?>