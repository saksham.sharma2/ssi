<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== banner_section_start ==== -->
<section class="banner_section_other" style="background-image: url('./images/layer9.png')">
    <div class="banner_area">
        <div class="inner_area">
            <p>OUR SERVICES</p>
        </div>
    </div>
</section>
<!-- ==== banner_section_end ==== -->

<!-- ==== service_section1_start ==== -->
<section class="service_section pd_top">
    <div class="box_area">
        <div class="images">
            <img src="images/layer2.png">
        </div>
    </div>
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="content_area">
                        <div class="heading">
                            <h3>Automation Solutions</h3>
                        </div>
                        <div class="content">
                            <p>The SSP System services division includes experienced engineering resources to undertake the full 
                                spectrum of automation project execution services. Local support is the key factor for selecting 
                                suppliers and vendors that sets us apart from the competition. We stay closely associated with the 
                                customer long after the process has started up. Seven Seas Petroleum supports all customers in Oman 
                                for Emerson's DeltaV Process Control System & DeltaV SIS for the following: 
                            </p>
                            <h6>Control System Design & Engineering</h6>
                            <h6>Control System installation & commissioning</h6>
                            <h6>Modification & expansion of control system</h6>
                            <h6>Preventive Maintenance</h6>
                            <h6>Corrective Maintenance</h6>
                            <h6>Emergency Call Site Support</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== service_section1_end ==== -->

<!-- ==== service_section2_start ==== -->
<section class="service_section">
    <div class="box_area el">
        <div class="images">
            <img src="images/layer3.png">
        </div>
    </div>
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">  
                    <div class="content_area  left_side">
                        <div class="heading">
                            <h3>Digitalisation</h3>
                        </div>
                        <div class="content">
                            <p>
                                Due to growth in the operations  complexity, the industry has been 
                                experiencing equally complex problems that threaten performance and 
                                reliability – but because of their multifaceted nature, the causes of 
                                these problems can be difficult to identify and resolve. 
                            </p>
                            <p>
                                Seven Seas Petroleum has access to various segments of a extensive 
                                Digitalisation Portfolio that can assist the industry  by transforming 
                                operational and manufacturing data into actionable information that can 
                                be used to ensure operational health, keep production on target and meet KPI goals. 
                            </p>
                            <p>
                                Using Artificial Intelligence and Machine Learning capabilities, we are geared to 
                                provide our clients with the most prominent solutions and services from niche 
                                applications such as instrumentation, rotating and capital equipment performance 
                                to complex processes found in refineries and energy managment solutions. Together 
                                with our partnerships, we aim to add value by enabling decision makers to have access 
                                to the right information at the right time to make the best choice to achieve operational excellence.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                   
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== service_section2_end ==== -->

<!-- ==== service_section3_start ==== -->
<section class="service_section">
    <div class="box_area">
        <div class="images">
            <img src="images/layer4.png">
        </div>
    </div>
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="content_area instru_padding">
                        <div class="heading">
                            <h3>Instrumentation Solutions</h3>
                        </div>
                        <div class="content">
                            <div class="inner_content">
                                <div class="left">
                                    <h6>Control & Safety Systems</h6>
                                    <h6>Asset Management</h6>
                                    <h6>Measurement Solutions</h6>
                                </div>
                                <div class="right">
                                    <h6>Valves, Actuators & Regulators</h6>
                                    <h6>Solenoids & Pneumatics</h6>
                                    <h6>Services & Consulting</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== service_section3_end ==== -->

<!-- ==== service_section4_start ==== -->
<section class="service_section">
    <div class="box_area el">
        <div class="images">
            <img src="images/layer7.png">
        </div>
    </div>
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="content_area left_side field_service_padding">
                        <div class="heading">
                            <h3>Field Services</h3>
                        </div>
                        <div class="content">
                            <p>Installation, commissioning and maintenance for the various 
                                instruments such as temperature transmitters, pressure transmitters, 
                                level transmitters (Guided Wave Radar and Pressure type level transmitters), 
                                Flow transmitters (Coriolis Flow Meter, Magnetic Flow Meter, Vortex Flow Meter, 
                                Multi-Variable Flow Meter and DP type Flow Meter), Gas Chromotograph. Flow Computers, 
                                (S600, S600+, FloBoss 107 and ROC 800), wireless (Gateway Wireless Transmitters and 
                                Wired Transmitters used with THUMB) and skids used for batch processing as well as for Fiscal metering.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                   
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== service_section4_end ==== -->

<!-- ==== service_section5_start ==== -->
<section class="service_section">
    <div class="box_area">
        <div class="images">
        <img src="images/layer6.png">
        </div>
    </div>
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="content_area turnkey_padding">
                        <div class="heading">
                            <h3>Turnkey Packages</h3>
                        </div>
                        <div class="content">
                            <p>Engineered Solutions is one of the two verticals of Seven Seas 
                                Petroleum that serves the local industry with custom- built capital 
                                equipment. It leverages the existing strong relationships that Seven 
                                Seas Petroleum has with many world renowned manufacturers & solution 
                                providers over several years and also develops new relationships in order 
                                to cater to the ever growing Oman industrial base.  
                            </p>
                            <p><span>There are six broad subdivisions in the Engineered Solutions Vertical:</span></p>
                            <p>Mechanical rotating equipment covering gas compressors, pumps, mechanical seals, couplings, 
                                flare gas recovery packages.
                            </p>
                            <p>
                                Mechanical static equipment covering seamless CRA pipes and tubes, cladded pipes, ejector 
                                solutions for flare gas recovery and production boosting. 
                            </p>
                            <p>
                                Process equipment like gas conditioning / Filtration / Sweetening / dehydration units, produced 
                                water treatment packages, oil train and gas train equipment and early production facilities.
                            </p>
                            <p>
                                Automation and electrical covering PLC based automation systems, electrical automation systems, production 
                                technologies, and electrical equipment. 
                            </p>
                            <p>
                                Integrated wellhead control panels and chemical injection skids.
                            </p>
                            <p>
                                DBOOM facilties for hydrocarbon production and power generation.
                            </p>
                            <p>
                                Sustainable Energy Solutions.
                            </p>
                            <p>
                                Flare Gas Recovery and monetization. 
                            </p>
                            <p>
                                Green Chemical and Hydrocarbon.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== service_section5_end ==== -->

<!-- ==== service_section6_start ==== -->
<section class="service_section">
    <div class="box_area el">
        <div class="images">
            <img src="images/layer5.png">
        </div>
    </div>
    <div class="service_area">
        <div class="container">
            <div class="row">
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 ">
                    <div class="content_area left_side mechanical_padding">
                        <div class="heading">
                            <h3>Mechanical Seal Refurbishment Workshop</h3>
                        </div>
                        <div class="content">
                            <p>The facility aims to enhance local services capability within 
                                Oman and ICV (In-Country Value), along with quick turnaround of 
                                repairs/refurbishment, for our clients in the region. This facility 
                                provides a platform to develop technical skills & experience of young 
                                Omani Engineers/Technicians in Mechanical seals services & maintenance. 
                                Moving further, it is intended to increase job opportunities for Omani 
                                nationals with increasing number of repair contracts/agreements with 
                                local companies. The approach will save considerable time and money for 
                                our customers as Mechanical seals will be now be repaired within Oman without crossing borders.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xxl-6 col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                   
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== service_section6_end ==== -->

<!-- ==== footer === -->
<?php include('common/partner.php') ?>
<?php include('common/get_in_touch.php') ?>
<?php include('common/footer.php') ?>