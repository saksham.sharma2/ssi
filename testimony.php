<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Breadcame_Section_Start ==== -->
<section class="breadcame_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="breadcame_area">
                    <h2>TESTIMONY</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Breadcame_Section_End ==== -->

<!-- ==== Customer_said_Section_start ==== -->
<section class="customer_said_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_area">
                    <div class="header_area">
                        <p>Testimony</p>
                        <h4>WHAT CUSTOMERS SAID</h4>
                    </div>
                    <div class="customer_said_area">
                        <div class="container">
                            <div class="row">
                                <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="contents_area">
                                        <div class="project_slider">
                                            <div class="review_set">
                                                <div class="owl-carousel" id="testimonials">
                                                    <div class="item">
                                                        <div class="content_box">
                                                            <div class="content">
                                                                <div class="rating_icon">
                                                                    <ul>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="inner_area">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                                                        Maecenas gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="image">
                                                                <img src="images/project1.png" alt="..." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="content_box">
                                                            <div class="content">
                                                                <div class="rating_icon">
                                                                    <ul>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="inner_area">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                                                        Maecenas gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="image">
                                                                <img src="images/team1.png" alt="..." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="content_box">
                                                            <div class="content">
                                                                <div class="rating_icon">
                                                                    <ul>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="inner_area">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                                                        Maecenas gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="image">
                                                                <img src="images/team1.png" alt="..." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="item">
                                                        <div class="content_box">
                                                            <div class="content">
                                                                <div class="rating_icon">
                                                                    <ul>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                        <li><i class="fas fa-star"></i></li>
                                                                    </ul>
                                                                </div>
                                                                <div class="inner_area">
                                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
                                                                        Maecenas gravida luctus ipsum ut sodales. Aenean diam felis, sollicitudin.
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <div class="image">
                                                                <img src="images/team1.png" alt="..." />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Customer_said_Section_End ==== -->

<!-- ==== Award_Section_Start ==== -->
<section class="award_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="award_area">
                    <div class="header">
                        <p>Awards</p>
                        <h4>SEVEN SEAS PETROLEUM LLC AWARDS</h4>
                        <h6>
                            We strive to achieve awards as a medium of measuring and recognizing our talents and 
                            efforts that we have put through our principles, towards our customers , and most importantly towards
                            bringing innovative solutions to our country and the GCC.
                        </h6>
                    </div>
                    <div class="image_area">
                        <img src="images/award.png" alt="..." />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Award_Section_End ==== -->

<!-- ==== Certification_Section_Start ==== -->
<section class="award_section top_space">
    <div class="container">
        <div lcass="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="award_area">
                    <div class="header">
                        <p>Certifications</p>
                        <h4>QUALITY CERTIFICATIONS</h4>
                        <h6>
                            have a look at some of the global renowned certifications that we have earned for our 
                            various facilities. The certification’s processes are put into place to insure quality management 
                            control and high ethical standards that we adhere by at Seven Seas Petroleum.
                        </h6>
                    </div>
                    <div class="certification_area">
                        <div class="row">
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="image">
                                    <img src="images/certificate.png" alt="..." />
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="image">
                                    <img src="images/certificate.png" alt="..." />
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="image">
                                    <img src="images/certificate.png" alt="..." />
                                </div>
                            </div>
                            <div class="col-xxl-3 col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                                <div class="image">
                                    <img src="images/certificate.png" alt="..." />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Certification_Section_End ==== -->





<!-- ==== footer === -->
<?php include('common/footer.php') ?>