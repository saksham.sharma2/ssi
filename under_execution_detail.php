<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== Slider_Section_Start ==== -->
<section class="slider_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="detail_slider sticky-md-top">
                    <div class="detail_page_start">
                        <div class="product_images">
                            <div id="sync1" class="owl-carousel-cc">
                                <div class="item">
                                    <div class="single_image">
                                        <img src="images/project_img.png" alt="..." />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="single_image">
                                        <img src="images/project_img.png" alt="..." />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="single_image">
                                        <img src="images/project_img.png" alt="..." />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="single_image">
                                        <img src="images/project_img.png" alt="..." />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="single_image">
                                        <img src="images/project_img.png" alt="..." />
                                    </div>
                                </div>
                            </div>
                            <div id="sync2" class="owl-carousel-cc">
                                    <div class="item">
                                        <div class="botttom_image">
                                            <img src="images/project_img.png" alt="..." />
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="botttom_image">
                                            <img src="images/project_img.png" alt="..." />
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="botttom_image">
                                            <img src="images/project_img.png" alt="..." />
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="botttom_image">
                                            <img src="images/project_img.png" alt="..." />
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="botttom_image">
                                            <img src="images/project_img.png" alt="..." />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Slider_Section_End ==== -->

<!-- ==== Project_Section_Start ==== -->
<section class="project_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_area">
                    <div class="header">
                        <h3>Project 1</h3>
                    </div>
                    <div class="content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet 
                            luctus venenatis, lectus magna fringilla urna, porttitorLorem ipsum dolor sit amet, 
                            consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla 
                            urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet 
                            luctus venenatis, lectus magna fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing 
                            elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, porttitorLorem ipsum dolor 
                            sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla 
                            urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus 
                            venenatis, lectus magna fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing elit 
                            ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, porttitorLorem ipsum dolor sit 
                            amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, 
                            porttitor
                        </p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna 
                            fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus 
                            venenatis, lectus magna fringilla urna, porttitorLorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam,
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Project_Section_End ==== -->

<!-- ==== Related_Project_Section_Start ==== -->
<section class="related_project_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="contents_area">
                    <div class="header">
                        <h4>Related Projects</h4>
                    </div>
                    <div class="project_slider">
                        <div class="review_set">
                            <div class="owl-carousel" id="project">
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/project1.png" alt="..." />
                                        </div>
                                        <div class="content active">
                                            <div class="inner_area">
                                                <h4> Project 1</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                    purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                </p>
                                                <a href="under_execution_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/project2.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4> Project 2</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                    purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                </p>
                                                <a href="under_execution_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/project3.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4> Project 3</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                    purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                </p>
                                                <a href="under_execution_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/project4.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4> Project 4</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                    purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                </p>
                                                <a href="under_execution_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/project5.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4> Project 5</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                    purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                </p>
                                                <a href="under_execution_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="content_box">
                                        <div class="image">
                                            <img src="images/project4.png" alt="..." />
                                        </div>
                                        <div class="content">
                                            <div class="inner_area">
                                                <h4> Project 6</h4>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                    purus sit amet luctus venenatis, lectus magna fringilla urna, porttitor 
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, 
                                                </p>
                                                <a href="under_execution_detail.php">Read More<i class="far fa-arrow-right ps-2"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Related_Project_Section_End ==== -->




<!-- ==== footer === -->
<?php include('common/footer.php') ?>