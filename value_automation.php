<!-- ==== Header === -->
<?php include('common/header.php') ?>

<!-- ==== banner_section_start ==== -->
<section class="banner_section_other" style="background-image: url('./images/layer9.png')">
    <div class="banner_area">
        <div class="inner_area">
            <p>Value Automation Center</p>
        </div>
    </div>
</section>
<!-- ==== banner_section_end ==== -->

<!-- ==== Header_content_section_start ==== -->
<section class="header_content_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="content_area">
                    <p>Seven Seas Petroleum LLC's Valve Automation Centre (VAC), situated in Barka 
                        (Sultanate of Oman), is geared up to address our customer's most demanding valve 
                        automation needs with cost-effective and reliable solutions. We engineer, integrate, 
                        repair, refurbish, test and supply an executive line of Value Automation products including 
                        Pneumatic, Hydraulic, Electric actuators, HPUs and a complete line of Valve products.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Header_content_section_end ==== -->

<!-- ==== Image_section_start ==== -->
<section class="image_section" style="background-image: url('./images/layer8.png')">
</section>
<!-- ==== Image_section_end ==== -->

<!-- ==== Why_seven_seas_section_start ==== -->
<section class="why_seven_seas_section" >
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="inner_area">
                    <div class="row align-items-center">
                        <div class="col-xxl-7 col-xl-7 col-lg-7 col-md-12 col-sm-12 col-12">
                            <div class="content">
                                <h4>WHY SEVEN SEAS VAC...?</h4>
                                <p>Single point Responsibility for the engineered solutions supplied</p>
                                <p>Brings together valves & actuators from the world's leading manufacturers like 
                                    Emerson Valve Automation, Fisher, Calobri, Della Foglia, Orsenigo, ATV, EIM etc.</p>
                                <p>State-of-the-art, fully computerized Ventil test bench, sophisticated machinery & precision 
                                    tools for integrated & testing of Control Valves, ESD Valves, Relief Valves, Gas Sampling Test Cylinder</p>
                                <p>Highly skilled, experienced & dedicated team backed-by field-proven application & engineering support</p>  
                                <p>Certified to ISO 9001 Quality Standards</p>  
                                <p>Qualified Inspected & Quality Assurance & Control throughout</p>
                                <p>Spacious Storage facility</p>
                                <p>Conveniently located</p>
                                <p>Fisher Authorized Service Provider</p>
                                <p>PDO Approved Valve Automation Centre in Sultanate of Oman</p>
                            </div>
                        </div>
                        <div class="col-xxl-5 col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                            <div class="image_area">
                                <img src="images/layer10.png" alt="..." />
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Why_seven_seas_section_end ==== -->

<!-- ==== Testing_capabilities_section_start ==== -->
<section class="testing_capabilities_section">
    <div class="container">
        <div class="row">
            <div class="col-xxl-12 col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="testing_capabilities_area">
                    <div class="row">
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="inner_area">
                                <h5>TESTING CAPABILITIES:</h5>
                                <p>Hydro test</p>
                                <p>Functional Test as per PDO DEP Requirement</p>
                                <p>Set Pressure Test</p>
                                <p>Re-seat or blow down pressure Test</p>
                                <p>Torque Test</p>
                                <p>Stroking Time Test as per PDO DEP Requirement / Client Requirement</p>
                                <p>Performance & Mechanical Operation Test as per PDO DEP Requirement</p>
                                <p>Stall Test as per PDO DEP Requirement</p>
                                <p>Gas Sampling Cylinder Testing</p>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                            <div class="inner_area">
                                <h5>SERVICES (Valve Automation)</h5>
                                <p>Supply of complete assembled units</p>
                                <p>Design and Fabrication of adaptor kits and interfaces</p>
                                <p>Installation and on-site commissioning support</p>
                                <p>Training</p>
                                <h6>SERVICES (RETROFIT)</h6>
                                <p>Survey and Installation</p>
                                <p>On-site retrofits of Actuators on existing Manual Valves</p>
                                <p>On-site replacement of obsolete actuators</p>
                            </div>
                        </div>
                        <div class="col-xxl-4 col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                            <div class="inner_area">
                                <h5>MAINTENANCE, REPAIR & REFURBISHMENT</h5>
                                <p>Yearly Maintenance Contract</p>
                                <p>Turnarounds</p>
                                <p>Valve Repair and Refurbishment</p>
                                <p>Certification</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ==== Testing_capabilities_section_end ==== -->

<!-- ==== footer === -->
<?php include('common/partner.php') ?>
<?php include('common/get_in_touch.php') ?>
<?php include('common/footer.php') ?>